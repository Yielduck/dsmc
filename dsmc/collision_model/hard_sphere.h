#pragma once
#include <dsmc/particle.h>
#include <dsmc/utils/uniform_vec3.h>
#include <numbers>
namespace dsmc::collision_model
{

template<typename T>
    requires requires(typename T::Mixture::template Substance<0> S)
    {
        {T::template diameter<decltype(S), decltype(S)>}
            -> std::convertible_to<dim::scalar<dim::Length>>;
    }
struct HardSphere
{
    using Table = T;
    template<dsmc::concepts::Substance S1, dsmc::concepts::Substance S2>
    static dim::scalar<dim::Area> crossSection(dsmc::Particle<S1> const &, dsmc::Particle<S2> const &) noexcept
    {
        constexpr dim::scalar<dim::Length> d = Table::template diameter<S1, S2>;
        return std::numbers::pi_v<dim::float_t> * d * d;
    }
    template<dsmc::concepts::Substance S1, dsmc::concepts::Substance S2>
    static void collide(dsmc::Particle<S1> &p1, dsmc::Particle<S2> &p2) noexcept
    {
        constexpr dim::scalar<dim::Mass> m1 = S1::molecularMass;
        constexpr dim::scalar<dim::Mass> m2 = S2::molecularMass;
        constexpr dim::scalar<dim::None> M1 = m1 / (m1 + m2);
        constexpr dim::scalar<dim::None> M2 = m2 / (m1 + m2);

        dim::vector<dim::None> const n = dsmc::utils::uniform_vec3(dsmc::utils::rng);

        dim::vector<dim::Velocity> const vm = M1 * p1.v + M2 * p2.v;
        dim::scalar<dim::Velocity> const vr = dim::length(p1.v - p2.v);

        p1.v = vm + (M2 * vr) * n;
        p2.v = vm - (M1 * vr) * n;
    }
};

} // namespace dsmc::collision_model
