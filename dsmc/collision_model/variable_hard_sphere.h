#pragma once
#include "hard_sphere.h"
#include <dim/constant.h>
namespace dsmc::collision_model
{

template<typename T>
    requires requires(typename T::Mixture::template Substance<0> S)
    {
        {T::template diameter   <decltype(S), decltype(S)>}
            -> std::convertible_to<dim::scalar<dim::Length>>;
        {T::template temperature<decltype(S), decltype(S)>}
            -> std::convertible_to<dim::scalar<dim::Temperature>>;
        {T::template omega      <decltype(S), decltype(S)>}
            -> std::convertible_to<dim::scalar<dim::None>>;
    }
struct VariableHardSphere
{
    using Table = T;
    template<dsmc::concepts::Substance S1, dsmc::concepts::Substance S2>
    static dim::scalar<dim::Area> crossSection(dsmc::Particle<S1> const &p1, dsmc::Particle<S2> const &p2) noexcept
    {
        constexpr dim::scalar<dim::Length> dRef = Table::template diameter<S1, S2>;
        constexpr dim::scalar<dim::Temperature> TRef = Table::template temperature<S1, S2>;
        constexpr dim::scalar<dim::None> omega = Table::template omega<S1, S2>;

        constexpr dim::scalar<dim::Mass> m1 = S1::molecularMass;
        constexpr dim::scalar<dim::Mass> m2 = S2::molecularMass;
        constexpr dim::scalar<dim::Mass> m = m1 * m2 / (m1 + m2);

        dim::vector<dim::Velocity> const v = p1.v - p2.v;
        return std::numbers::pi_v<dim::float_t>
             * dRef * dRef
             * dim::pow(m * dim::dot(v, v) / (2 * dim::constant::k_B * TRef), 0.5 - omega)
             / dim::tgamma(2.5 - omega);
    }
    template<dsmc::concepts::Substance S1, dsmc::concepts::Substance S2>
    static void collide(dsmc::Particle<S1> &p1, dsmc::Particle<S2> &p2) noexcept
    {
        HardSphere<Table>::template collide(p1, p2);
    }
};

} // namespace dsmc::collision_model
