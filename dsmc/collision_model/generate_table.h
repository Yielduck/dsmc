#pragma once
#include <dim/constant.h>

#include <array>
#include <numbers>
#include <ostream>
#include <ranges>
#include <string>

namespace dsmc::collision_model
{

struct SubstanceInfo
{
    std::string name;
    dim::scalar<dim::Mass> mass;
    dim::scalar<dim::Temperature> TRef;
    dim::scalar<dim::Viscosity> muRef;
    dim::scalar<dim::None> omega;
    dim::scalar<dim::None> Sc;
};

struct SubstancePairInfo
{
    dim::scalar<dim::Temperature> TRef;
    dim::scalar<dim::Concentration> nRef;
    dim::scalar<dim::div(dim::Area, dim::Time)> DRef;
    dim::scalar<dim::None> omega;
};
    
template<std::size_t N>
void generateTable
(
    std::array<SubstanceInfo, N> const &info,
    std::array<SubstancePairInfo, (N * (N - 1)) / 2> const &pairInfo,
    /* example: N = 5, pairInfo.size() = 10;
       / - 0 1 2 3 \
       | - - 4 5 6 |
       | - - - 7 8 |
       | - - - - 9 |
       \ - - - - - /
    */
    std::ostream &out
)
{
    constexpr dim::scalar<dim::None> pi = std::numbers::pi_v<dim::float_t>;

    struct TableEntry
    {
        dim::scalar<dim::Length> d;
        dim::scalar<dim::Temperature> T;
        dim::scalar<dim::None> omega;
        dim::scalar<dim::None> alpha;
    } entry[N][N];
    for(std::size_t i : std::views::iota(0ull, N))
        for(std::size_t j : std::views::iota(0ull, N))
            if(i == j)
            {
                auto const &[name, m, TRef, muRef, omega, Sc] = info[i];
                auto const E = m * dim::constant::k_B * TRef / pi;
                dim::scalar<dim::None> const alpha = 2 / (3 * (7 - 2 * omega) * Sc / 5 - 1);
                entry[i][i] = TableEntry
                {
                    .d = dim::sqrt
                    (
                        5 * (alpha + 1) * (alpha + 2) * dim::sqrt(E)
                      / (4 * alpha * (5 - 2 * omega) * (7 - 2 * omega) * muRef)
                    ),
                    .T = TRef,
                    .omega = omega,
                    .alpha = alpha,
                };
            }

    std::size_t k = 0;
    for(std::size_t i : std::views::iota(0ull, N - 1))
    {
        for(std::size_t j : std::views::iota(i + 1, N))
        {
            dim::scalar<dim::Mass> const m1 = info[i].mass;
            dim::scalar<dim::Mass> const m2 = info[j].mass;
            dim::scalar<dim::Mass> const m = m1 * m2 / (m1 + m2);
            dim::scalar<dim::Length> const d12 = (entry[i][i].d + entry[j][j].d) / 2;
            auto const &[TRef, nRef, DRef, omega] = pairInfo[k++];

            auto const E = 2 * pi * dim::constant::k_B * TRef / m;
            entry[i][j] = TableEntry
            {
                .d = d12,
                .T = TRef,
                .omega = omega,
                .alpha = -1 + 8 * (5 - 2 * omega) * nRef * DRef * pi * d12 * d12 / 3 / dim::sqrt(E),
            };
            entry[j][i] = entry[i][j];
        }
    }

    out << "#pragma once" << std::endl;
    out << "#include <dsmc/mixture.h>" << std::endl;
    for(auto const &[name, mass, T, mu, omega, Sc] : info)
    {
        out << "struct " << name << std::endl;
        out << "{" << std::endl;
        out << "    static constexpr dim::scalar<dim::Mass> molecularMass = " << mass.value << ";" << std::endl;
        out << "};" << std::endl;
    }

    out << "struct Table" << std::endl;
    out << "{" << std::endl;

    out << "    using Mixture = dsmc::Mixture<";
    out << info[0].name;
    for(auto const &[name, mass, T, mu, omega, Sc] : info | std::views::drop(1))
        out << ", " << name;
    out << ">;" << std::endl;
    out << std::endl;

    out << "    static constexpr pp::type_index N = Mixture::substanceCount;" << std::endl;
    out << std::endl;

    out << "    template<dsmc::concepts::Substance S>" << std::endl;
    out << "    static constexpr pp::type_index I = Mixture::template indexOf<S>;" << std::endl;
    out << std::endl;

    out << "    static constexpr dim::scalar<dim::Length> dRef[N] =" << std::endl;
    out << "    {" << std::endl;
    for(std::size_t i : std::views::iota(0ull, N))
        out << "        " << entry[i][i].d.value << "," << std::endl;
    out << "    };" << std::endl;

    out << "    static constexpr dim::scalar<dim::Temperature> TRef[N][N] =" << std::endl;
    out << "    {" << std::endl;
    for(auto const &row : entry)
    {
        out << "        {" << row[0].T.value;
        for(TableEntry const &e : row | std::views::drop(1))
            out << ", " << e.T.value;
        out << "}," << std::endl;
    }
    out << "    };" << std::endl;

    out << "    static constexpr dim::scalar<dim::None> omegaRef[N][N] =" << std::endl;
    out << "    {" << std::endl;
    for(auto const &row : entry)
    {
        out << "        {" << row[0].omega.value;
        for(TableEntry const &e : row | std::views::drop(1))
            out << ", " << e.omega.value;
        out << "}," << std::endl;
    }
    out << "    };" << std::endl;

    out << "    static constexpr dim::scalar<dim::None> alphaRef[N][N] =" << std::endl;
    out << "    {" << std::endl;
    for(auto const &row : entry)
    {
        out << "        {" << row[0].alpha.value;
        for(TableEntry const &e : row | std::views::drop(1))
            out << ", " << e.alpha.value;
        out << "}," << std::endl;
    }
    out << "    };" << std::endl;
    out << std::endl;

    out << "    template<dsmc::concepts::Substance S1, dsmc::concepts::Substance S2>" << std::endl;
    out << "    static constexpr dim::scalar<dim::Length> diameter = (dRef[I<S1>] + dRef[I<S2>]) / 2;" << std::endl;
    out << "    template<dsmc::concepts::Substance S1, dsmc::concepts::Substance S2>" << std::endl;
    out << "    static constexpr dim::scalar<dim::Temperature> temperature = TRef[I<S1>][I<S2>];" << std::endl;
    out << "    template<dsmc::concepts::Substance S1, dsmc::concepts::Substance S2>" << std::endl;
    out << "    static constexpr dim::scalar<dim::None> omega = omegaRef[I<S1>][I<S2>];" << std::endl;
    out << "    template<dsmc::concepts::Substance S1, dsmc::concepts::Substance S2>" << std::endl;
    out << "    static constexpr dim::scalar<dim::None> alpha = alphaRef[I<S1>][I<S2>];" << std::endl;

    out << "};" << std::endl;
}

} // namespace dsmc::collision_model
