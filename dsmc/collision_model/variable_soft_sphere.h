#pragma once
#include "variable_hard_sphere.h"
#include <dsmc/utils/to_normal_space.h>
namespace dsmc::collision_model
{

template<typename T>
    requires requires(typename T::Mixture::template Substance<0> S)
    {
        {T::template diameter   <decltype(S), decltype(S)>}
            -> std::convertible_to<dim::scalar<dim::Length>>;
        {T::template temperature<decltype(S), decltype(S)>}
            -> std::convertible_to<dim::scalar<dim::Temperature>>;
        {T::template omega      <decltype(S), decltype(S)>}
            -> std::convertible_to<dim::scalar<dim::None>>;
        {T::template alpha      <decltype(S), decltype(S)>}
            -> std::convertible_to<dim::scalar<dim::None>>;
    }
struct VariableSoftSphere
{
    using Table = T;
    template<dsmc::concepts::Substance S1, dsmc::concepts::Substance S2>
    static dim::scalar<dim::Area> crossSection(dsmc::Particle<S1> const &p1, dsmc::Particle<S2> const &p2) noexcept
    {
        return VariableHardSphere<Table>::template crossSection(p1, p2);
    }
    template<dsmc::concepts::Substance S1, dsmc::concepts::Substance S2>
    static void collide(dsmc::Particle<S1> &p1, dsmc::Particle<S2> &p2) noexcept
    {
        constexpr dim::scalar<dim::Mass> m1 = S1::molecularMass;
        constexpr dim::scalar<dim::Mass> m2 = S2::molecularMass;
        constexpr dim::scalar<dim::None> M1 = m1 / (m1 + m2);
        constexpr dim::scalar<dim::None> M2 = m2 / (m1 + m2);

        dim::vector<dim::Velocity> const vm = M1 * p1.v + M2 * p2.v;
        dim::scalar<dim::Velocity> const vr = dim::length(p1.v - p2.v);

        auto const S = dsmc::utils::to_normal_space(dim::normalize(p1.v - p2.v));
        dim::float_t const rand[2] =
        {
            dsmc::utils::canonical(dsmc::utils::rng),
            dsmc::utils::canonical(dsmc::utils::rng),
        };

        dim::float_t const cos_theta = 2 * std::pow(rand[0], 1 / Table::template alpha<S1, S2>.value) - 1;
        dim::float_t const sin_theta = std::sqrt(1 - cos_theta * cos_theta);
        dim::float_t const phi = rand[1] * 2 * std::numbers::pi_v<dim::float_t>;
        dim::vector<dim::None> const n =
        {
            sin_theta * std::cos(phi),
            sin_theta * std::sin(phi),
            cos_theta,
        };

        p1.v = vm + (M2 * vr) * (S * n);
        p2.v = vm - (M1 * vr) * (S * n);
    }
};

} // namespace dsmc::collision_model
