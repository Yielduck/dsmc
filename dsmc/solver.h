#pragma once
#include <dsmc/cell.h>
#include <dsmc/collide.h>
#include <dsmc/move.h>
#include <dsmc/sort.h>
#include <dsmc/concepts/boundary.h>
#include <dsmc/concepts/collision_model.h>
#include <dsmc/concepts/mesh.h>
#include <dsmc/utils/thread_pool.h>
#include <mutex>
#include <vector>
namespace dsmc
{

class Solver
{
    dsmc::utils::ThreadPool threadPool;
    std::vector<std::mutex> cellMutex;

public:
    Solver(unsigned int const threadCount = std::thread::hardware_concurrency()) noexcept
        : threadPool(threadCount)
    {}

    template
    <
        dsmc::concepts::CollisionModel CollisionModel,
        dsmc::concepts::Boundary Boundary,
        dsmc::concepts::Mesh Mesh,
        dsmc::concepts::Substance... Ss
    >
    dim::scalar<dim::None> step
        // returns the maximum [collisionCount / particleCount] ratio among all cells
    (
        dim::scalar<dim::Time> const dt,
        Boundary &boundary,
        Mesh const &mesh,
        std::vector<dsmc::Cell<Ss...>> &ensemble
    ) noexcept
    {
        using Mixture = dsmc::Mixture<Ss...>;
        using Cell = dsmc::Cell<Ss...>;

        if(cellMutex.size() < mesh.cells())
            cellMutex = std::vector<std::mutex>(mesh.cells());

        // move
        for(Cell &cell : ensemble)
            threadPool.execute
            (
                [&]() noexcept
                    -> void
                {
                    cell.template forEachCloud
                    (
                        [&]<dsmc::concepts::Substance S>(dsmc::Cloud<S> &cloud) noexcept
                            -> void
                        {
                            std::size_t i = 0;
                            std::size_t sz = cloud.size();
                            while(i < sz)
                            {
                                auto const optP = dsmc::move(cloud[i], dt, boundary);
                                if(optP)
                                    cloud[i++] = *optP;
                                else
                                    cloud[i] = cloud[--sz];
                            }
                            cloud.resize(sz);
                        }
                    );
                }
            );
        threadPool.wait();

        // inject boundary particles, sort all particles with mesh
        Mixture::template forEachSubstance
        (
            [&]<dsmc::concepts::Substance S>() noexcept
                -> void
            {
                std::vector<dsmc::Cloud<S>> newCloud(mesh.cells());
                auto const redistribute = [&](auto &&from) noexcept
                    -> void
                {
                    for(auto const &[cellID, particleRange] : dsmc::sort(std::forward<dsmc::Cloud<S>>(from), mesh))
                    {
                        std::lock_guard<std::mutex> const guard(cellMutex[cellID]);
                        newCloud[cellID].insert
                        (
                            newCloud[cellID].end(),
                            std::ranges::cbegin(particleRange),
                            std::ranges::cend  (particleRange)
                        );
                    }
                };
                threadPool.execute
                (
                    [&, redistribute]() noexcept
                        -> void
                    {
                        auto particle = boundary.template spawn<S>(dt) | std::views::transform
                        (
                            [&](dsmc::Particle<S> const &p) noexcept
                                -> std::optional<dsmc::Particle<S>>
                            {
                                return dsmc::move(p, dt * dsmc::utils::canonical(), boundary);
                            }
                        ) | std::views::filter
                        (
                            [](std::optional<dsmc::Particle<S>> const &opt) noexcept
                                -> bool
                            {
                                return static_cast<bool>(opt);
                            }
                        ) | std::views::transform
                        (
                            [](std::optional<dsmc::Particle<S>> const &opt) noexcept
                                -> dsmc::Particle<S>
                            {
                                return *opt;
                            }
                        ) | std::views::common;
                        redistribute(dsmc::Cloud<S>(std::ranges::begin(particle), std::ranges::end(particle)));
                    }
                );
                for(Cell &cell : ensemble)
                    threadPool.execute
                    (
                        [&, redistribute]() noexcept
                            -> void
                        {
                            redistribute(std::move(cell.template cloud<S>()));
                        }
                    );
                threadPool.wait();

                for(auto const cellID : std::views::iota(0u, mesh.cells()))
                    std::swap(ensemble[cellID].template cloud<S>(), newCloud[cellID]);
            }
        );

        // collide
        dim::float_t ratio = 0;
        std::mutex ratioMutex;
        for(auto const cellID : std::views::iota(0u, mesh.cells()))
            threadPool.execute
            (
                [&, cellID]() noexcept
                    -> void
                {
                    auto const collisionCount = dsmc::collide<CollisionModel>(ensemble[cellID], dt);

                    dim::float_t collisionRatio[Mixture::substanceCount] = {};
                    ensemble[cellID].template forEachCloud
                    (
                        [&, i = 0u](auto const &cloud) mutable noexcept
                        {
                            collisionRatio[i] = cloud.size() == 0
                                ? 0
                                : static_cast<dim::float_t>(collisionCount[i])
                                / static_cast<dim::float_t>(cloud.size());
                            ++i;
                        }
                    );

                    std::lock_guard<std::mutex> const guard(ratioMutex);
                    ratio = std::max(ratio, std::ranges::max(collisionRatio));
                }
            );
        threadPool.wait();

        return ratio;
    }
};

} // namespace dsmc
