#pragma once
#include <dsmc/particle.h>
#include <dsmc/concepts/boundary.h>
namespace dsmc
{

template
<
    dsmc::concepts::Substance S,
    dsmc::concepts::Boundary Boundary
>
std::optional<dsmc::Particle<S>> move
(
    dsmc::Particle<S> p,
    dim::scalar<dim::Time> t,
    Boundary &boundary
) noexcept
{
    while(true)
    {
        std::optional<dsmc::surface::Hit> const optHit = boundary.surfaceHit(p.r, p.v);
        if(static_cast<bool>(optHit) == false || optHit->t > t)
            return
            {{
                 .r = p.r + p.v * t,
                 .v = p.v,
            }};
        auto const &[escaped, dt, r, v] = boundary.hit(p);
        if(escaped)
            return std::nullopt;
        t   -= dt;
        p.r  = r;
        p.v  = v;
    }
}

} // namespace dsmc
