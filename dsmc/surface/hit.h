#pragma once
#include <dim/vector.h>
namespace dsmc::surface
{

struct Hit
{
    dim::scalar<dim::Time> t;
    dim::vector<dim::Length> r;
};

} // namespace dsmc::surface
