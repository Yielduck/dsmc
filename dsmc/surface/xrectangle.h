#pragma once
#include <dsmc/distribution/interval.h>
#include <dsmc/surface/hit.h>
#include <dsmc/utils/rng.h>
#include <limits>
#include <optional>
namespace dsmc::surface
{

template<bool faceFront>
struct XRectangle
{
    dim::vector<dim::Length> r;
    dim::scalar<dim::Length> dy, dz;

    using L = std::numeric_limits<dim::float_t>;

    std::optional<dsmc::surface::Hit> hit(dim::vector<dim::Length> const &pr, dim::vector<dim::Velocity> const &v) const noexcept
    {
        dim::scalar<dim::Time> const t = (r.x - pr.x) / v.x;
        if(t > 0 || r.x == pr.x)
            return
            {{
                .t = t,
                .r =
                {
                    std::nextafter(r.x.value, faceFront ? L::max() : L::min()),
                    pr.y + v.y * t,
                    pr.z + v.z * t,
                }
            }};
        else
            return std::nullopt;
    }
    dim::vector<dim::Length> point(dsmc::utils::RNG &generator) const noexcept
    {
        return
        {
            std::nextafter(r.x.value, faceFront ? L::max() : L::min()),
            dsmc::distribution::Interval(r.y, r.y + dy)(generator),
            dsmc::distribution::Interval(r.z, r.z + dz)(generator),
        };
    }
    dim::vector<dim::None> norm() const noexcept
    {
        return {faceFront ? 1 : -1, 0, 0};
    }
    dim::scalar<dim::Area> area() const noexcept
    {
        return dy * dz;
    }
};

} // namespace dsmc::surface
