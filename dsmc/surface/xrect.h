#pragma once
#include <dsmc/distribution/interval.h>
#include <dsmc/surface/hit.h>
#include <dsmc/utils/rng.h>
#include <limits>
#include <optional>
namespace dsmc::surface
{

struct XRect
{
    dim::vector<dim::Length> r;
    dim::scalar<dim::Length> dy, dz;

    std::optional<dsmc::surface::Hit> hit(dim::vector<dim::Length> const &pr, dim::vector<dim::Velocity> const &v) const noexcept
    {
        dim::scalar<dim::Time> const t = (r.x - pr.x) / v.x;
        if(t > 0)
            return
            {{
                .t = t,
                .r =
                {
                    r.x.value,
                    pr.y + v.y * t,
                    pr.z + v.z * t,
                }
            }};
        else
            return std::nullopt;
    }
    dim::vector<dim::Length> point(dsmc::utils::RNG &generator) const noexcept
    {
        return
        {
            r.x.value,
            dsmc::distribution::Interval(r.y, r.y + dy)(generator),
            dsmc::distribution::Interval(r.z, r.z + dz)(generator),
        };
    }
    dim::vector<dim::None> norm() const noexcept
    {
        return {1, 0, 0};
    }
    dim::scalar<dim::Area> area() const noexcept
    {
        return dy * dz;
    }
};

} // namespace dsmc::surface
