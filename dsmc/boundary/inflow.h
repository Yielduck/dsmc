#pragma once
#include <dsmc/mixture.h>
#include <dsmc/boundary/hit.h>
#include <dsmc/concepts/surface.h>
#include <dsmc/distribution/inflow.h>
#include <ranges>
#include <span>
namespace dsmc::boundary
{

template<dsmc::concepts::Surface Surface, dsmc::concepts::Substance... Ss>
struct Inflow
{
    using Mixture = dsmc::Mixture<Ss...>;
    static constexpr pp::type_index substanceCount = Mixture::substanceCount;

    Surface surface;

    dim::scalar<dim::Concentration> n[substanceCount];
    dim::scalar<dim::None> statWeight;
    dim::scalar<dim::Temperature> T;
    dim::vector<dim::Velocity> U;

    std::optional<dsmc::surface::Hit> surfaceHit(dim::vector<dim::Length> const &r, dim::vector<dim::Velocity> const &v) const noexcept
    {
        return surface.hit(r, v);
    }
    template<dsmc::concepts::Substance S>
    dsmc::boundary::Hit hit(dsmc::Particle<S> const p) noexcept
    {
        auto const [t, r] = *surfaceHit(p.r, p.v);
        return
        {
            .escaped = true,
            .t = t,
            .r = r,
            .v = p.v,
        };
    }
    template<dsmc::concepts::Substance S>
        requires(Mixture::template indexOf<S> != pp::not_found)
    auto spawn(dim::scalar<dim::Time> const dt) noexcept
    {
        dsmc::distribution::Inflow<S> uD = {surface.norm(), T, U};
        dim::scalar<dim::None> const sqrtpi = std::sqrt(std::numbers::pi_v<dim::float_t>);
        dim::scalar<dim::None> const N = n[Mixture::template indexOf<S>] * surface.area() * dt * uD.vT
            * (dim::exp(-uD.a * uD.a) + uD.a * sqrtpi * (1 + dim::erf(uD.a)))
            / (2 * sqrtpi * statWeight);

        return std::views::iota(0ull, std::poisson_distribution<std::size_t>(N.value)(dsmc::utils::rng)) | std::views::transform
        (
            [this, dt, uD](std::size_t) mutable noexcept
                -> dsmc::Particle<S>
            {
                return
                {
                    surface.point(dsmc::utils::rng),
                    uD(dsmc::utils::rng),
                };
            }
        );
    }
    template<dsmc::concepts::Substance S>
    std::span<dsmc::Particle<S> const, 0> spawn(dim::scalar<dim::Time>) noexcept
    {
        return {};
    }
};

} // namespace dsmc::boundary
