#pragma once
#include <dsmc/boundary/hit.h>
#include <dsmc/concepts/surface.h>
#include <dsmc/distribution/diffuse.h>
#include <optional>
#include <span>
namespace dsmc::boundary
{

template<dsmc::concepts::Surface Surface>
struct Diffuse
{
    Surface surface;
    dim::scalar<dim::Temperature> T;
    dim::vector<dim::Velocity> U;

    std::optional<dsmc::surface::Hit> surfaceHit(dim::vector<dim::Length> const &r, dim::vector<dim::Velocity> const &v) const noexcept
    {
        return surface.hit(r, v);
    }
    template<dsmc::concepts::Substance S>
    dsmc::boundary::Hit hit(dsmc::Particle<S> const &p) const noexcept
    {
        auto const [t, r] = *surfaceHit(p.r, p.v);
        dim::vector<dim::None> const n = surface.norm();
        return
        {
            .escaped = false,
            .t = t,
            .r = r,
            .v = dsmc::distribution::Diffuse<S>(n, T, U)(dsmc::utils::rng),
        };
    }
    template<dsmc::concepts::Substance S>
    std::span<dsmc::Particle<S>, 0> spawn(dim::scalar<dim::Time>) const noexcept
    {
        return {};
    }
};

} // namespace dsmc::boundary
