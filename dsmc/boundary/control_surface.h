#pragma once
#include <dsmc/mixture.h>
#include <dsmc/concepts/surface.h>
#include <span>
namespace dsmc::boundary
{

template<dsmc::concepts::Surface Surface, dsmc::concepts::Substance... Ss>
struct ControlSurface
{
    Surface surface;

    using Mixture = dsmc::Mixture<Ss...>;
    struct Info
    {
        std::size_t hitCount[2] = {0u, 0u};
        dim::scalar<dim::Velocity> vn[2] = {0, 0};
    } info[Mixture::substanceCount];

    std::optional<dsmc::surface::Hit> surfaceHit(dim::vector<dim::Length> const &r, dim::vector<dim::Velocity> const &v) const noexcept
    {
        return surface.hit(r, v);
    }
    template<dsmc::concepts::Substance S>
    dsmc::boundary::Hit hit(dsmc::Particle<S> const &p) noexcept
    {
        auto const [t, r] = *surfaceHit(p.r, p.v);
        constexpr pp::type_index I = Mixture::template indexOf<S>;
        if constexpr(I != pp::not_found)
        {
            dim::scalar<dim::Velocity> const vn = dim::dot(p.v, surface.norm());
            std::size_t const i = vn > 0 ? 0 : 1;
            info[I].hitCount[i] += 1;
            info[I].vn[i] += vn;
        }
        return
        {
            .escaped = false,
            .t = t,
            .r = r,
            .v = p.v,
        };
    }
    template<dsmc::concepts::Substance S>
    std::span<dsmc::Particle<S>, 0> spawn(dim::scalar<dim::Time>) noexcept
    {
        return {};
    }
};

} // namespace dsmc::boundary
