#pragma once
#include <dsmc/boundary/hit.h>
#include <dsmc/surface/hit.h>
#include <limits>
#include <optional>
#include <span>
namespace dsmc::boundary
{

template<unsigned int axle>
struct Symmetry
{
    dim::vector<dim::Length> r0, r1;

    std::optional<dsmc::surface::Hit> surfaceHit(dim::vector<dim::Length> r, dim::vector<dim::Velocity> const &v) const noexcept
    {
        dim::scalar<dim::Time> const t0 = (r0 - r)[axle] / v[axle];
        dim::scalar<dim::Time> const t1 = (r1 - r)[axle] / v[axle];

        dim::scalar<dim::Time> const t = t0 < t1 ? t1 : t0;

        using L = std::numeric_limits<dim::float_t>;

        r += v * t;
        r[axle] = t0 < t1
            ? std::nextafter(r0[axle].value, L::max())
            : std::nextafter(r1[axle].value, L::min());
        return
        {{
             .t = t,
             .r = r,
        }};
    }
    template<dsmc::concepts::Substance S>
    dsmc::boundary::Hit hit(dsmc::Particle<S> const &p) noexcept
    {
        auto const [t, r] = *surfaceHit(p.r, p.v);
        return
        {
            .escaped = false,
            .t = t,
            .r = r,
            .v = p.v,
        };
    }
    template<dsmc::concepts::Substance S>
    std::span<dsmc::Particle<S>, 0> spawn(dim::scalar<dim::Time>) noexcept
    {
        return {};
    }
};

} // namespace dsmc::boundary
