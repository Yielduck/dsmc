#pragma once
#include <dsmc/boundary/hit.h>
#include <dsmc/concepts/surface.h>
#include <optional>
#include <span>
namespace dsmc::boundary
{

template<dsmc::concepts::Surface Surface>
struct Specular
{
    Surface surface;

    std::optional<dsmc::surface::Hit> surfaceHit(dim::vector<dim::Length> const &r, dim::vector<dim::Velocity> const &v) const noexcept
    {
        return surface.hit(r, v);
    }
    template<dsmc::concepts::Substance S>
    dsmc::boundary::Hit hit(dsmc::Particle<S> const &p) noexcept
    {
        auto const [t, r] = *surfaceHit(p.r, p.v);
        dim::vector<dim::None> const n = surface.norm();
        return
        {
            .escaped = false,
            .t = t,
            .r = r,
            .v = p.v - 2 * dim::dot(p.v, n) * n,
        };
    }
    template<dsmc::concepts::Substance S>
    std::span<dsmc::Particle<S>, 0> spawn(dim::scalar<dim::Time>) noexcept
    {
        return {};
    }
};

} // namespace dsmc::boundary
