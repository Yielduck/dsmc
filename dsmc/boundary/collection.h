#pragma once
#include <dsmc/cloud.h>
#include <dsmc/concepts/boundary.h>
#include <algorithm>
#include <tuple>
namespace dsmc::boundary
{

template<dsmc::concepts::Boundary... Bs>
struct Collection : public std::tuple<Bs...>
{
    std::optional<dsmc::surface::Hit> surfaceHit(dim::vector<dim::Length> const &r, dim::vector<dim::Velocity> const &v) const noexcept
    {
        std::optional<dsmc::surface::Hit> optHit[sizeof...(Bs)];
        forEachBoundary
        (
            [&, i = 0u]<dsmc::concepts::Boundary B>(B const &boundary) mutable noexcept
                -> void
            {
                optHit[i++] = boundary.surfaceHit(r, v);
            }
        );
        return *std::ranges::min_element
        (
            optHit,
            [](auto const &opt1, auto const &opt2) noexcept
                -> bool
            {
                return static_cast<bool>(opt1)
                    ? (static_cast<bool>(opt2) ? opt1->t < opt2->t : true)
                    : false;
            }
        );
    }

    template<dsmc::concepts::Substance S>
    dsmc::boundary::Hit hit(dsmc::Particle<S> const &p) noexcept
    {
        std::optional<dsmc::surface::Hit> optHit[sizeof...(Bs)];
        forEachBoundary
        (
            [&optHit, &p, i = 0u]<dsmc::concepts::Boundary B>(B const &boundary) mutable noexcept
                -> void
            {
                optHit[i++] = boundary.surfaceHit(p.r, p.v);
            }
        );
        std::optional<dsmc::surface::Hit> * const pHit = std::ranges::min_element
        (
            optHit,
            [](auto const &opt1, auto const &opt2) noexcept
                -> bool
            {
                return static_cast<bool>(opt1)
                    ? (static_cast<bool>(opt2) ? opt1->t < opt2->t : true)
                    : false;
            }
        );
        return visit
        (
            [&p](auto &boundary) noexcept {return boundary.hit(p);},
            static_cast<std::size_t>(pHit - optHit)
        );
    }

    template<dsmc::concepts::Substance S>
    dsmc::Cloud<S> const &spawn(dim::scalar<dim::Time> const dt) noexcept
    {
        static thread_local dsmc::Cloud<S> cloud;
        cloud.clear();

        forEachBoundary
        (
            [dt]<dsmc::concepts::Boundary B>(B &boundary) noexcept
                -> void
            {
                auto R = boundary.template spawn<S>(dt) | std::views::common;
                cloud.insert(cloud.end(), std::ranges::begin(R), std::ranges::end(R));
            }
        );
        return cloud;
    }

    template<typename F>
    void forEachBoundary(F &&f) noexcept
    {
        std::apply
        (
            [&f](Bs &... args) noexcept
            {
                (..., f(args));
            },
            *static_cast<std::tuple<Bs...> *>(this)
        );
    }
    template<typename F>
    void forEachBoundary(F &&f) const noexcept
    {
        std::apply
        (
            [&f](Bs const &... args) noexcept
            {
                (..., f(args));
            },
            *static_cast<std::tuple<Bs...> const *>(this)
        );
    }
    template<typename F, std::size_t... I>
    auto visitImpl(F &&func, std::size_t const i, std::index_sequence<I...>) noexcept
    {
        auto * const pTuple = static_cast<std::tuple<Bs...> *>(this);
        using R = decltype(func(std::get<0>(*pTuple)));
        using VisitFunc = R(std::tuple<Bs...> &, F &&);
        static constexpr VisitFunc *fs[] =
        {
            +[](std::tuple<Bs...> &tup, F &&f) noexcept
                -> R
            {
                return f(std::get<I>(tup));
            }...
        };
        return fs[i](*pTuple, std::forward<F>(func));
    }
    template<typename F>
    auto visit(F &&func, std::size_t const i) noexcept
    {
        return visitImpl(std::forward<F>(func), i, std::make_index_sequence<sizeof...(Bs)>{});
    }
};

} // namespace dsmc::boundary
