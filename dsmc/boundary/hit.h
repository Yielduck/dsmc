#pragma once
#include <dim/vector.h>
namespace dsmc::boundary
{

struct Hit
{
    bool escaped = false;
    dim::scalar<dim::Time> t;
    dim::vector<dim::Length> r;
    dim::vector<dim::Velocity> v;
};

} // namespace dsmc::boundary
