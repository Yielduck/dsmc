#pragma once
#include <dsmc/cloud.h>
#include <pp/find_type.h>
#include <array>
namespace dsmc
{

template<dsmc::concepts::Substance... Ss>
class Cell : dsmc::Cloud<Ss>...
{
    std::array
    <
        dim::scalar<dim::mul(dim::Area, dim::Velocity)>,
        (sizeof...(Ss) * (sizeof...(Ss) + 1)) / 2
    > sigmaVr;

    template<dsmc::concepts::Substance S>
    static constexpr pp::type_index indexOf = pp::find_type<S>::template in<Ss...>;

public:
    dim::scalar<dim::Concentration> const weight; // particleStatWeight / cloudVolume;

    Cell(dim::scalar<dim::Concentration> const w) noexcept
        : sigmaVr{}
        , weight(w)
    {}

    template<dsmc::concepts::Substance S>
    dsmc::Cloud<S>       &cloud()       noexcept {return static_cast<dsmc::Cloud<S>       &>(*this);}
    template<dsmc::concepts::Substance S>
    dsmc::Cloud<S> const &cloud() const noexcept {return static_cast<dsmc::Cloud<S> const &>(*this);}

    template<typename Visitor>
    void forEachCloud(Visitor &&f)       noexcept {(..., f(cloud<Ss>()));}
    template<typename Visitor>
    void forEachCloud(Visitor &&f) const noexcept {(..., f(cloud<Ss>()));}

    template<dsmc::concepts::Substance S1, dsmc::concepts::Substance S2>
        requires(indexOf<S1> < sizeof...(Ss) && indexOf<S2> < sizeof...(Ss))
    auto &sigmaVrMax() noexcept
    {
        constexpr pp::type_index I = std::min(indexOf<S1>, indexOf<S2>);
        constexpr pp::type_index J = std::max(indexOf<S1>, indexOf<S2>);
        return sigmaVr[I * (sizeof...(Ss) - 1) - (I * (I - 1)) / 2 + J];
    }
};

} // namespace dsmc
