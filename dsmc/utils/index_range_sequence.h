#pragma once
#include <concepts>
#include <ranges>
namespace dsmc::utils
{

// {0, 0, 0, 1, 1, 2, 2, 3, 3, 4, 5} ->
// {0, {0, 3}}, {1, {3, 5}}, {2, {5, 7}}, {3, {7, 9}}, {4, {9, 10}}, {5, {10, 11}}
template<std::unsigned_integral uint, std::random_access_iterator I>
struct IndexRangeSequence : std::ranges::view_base
{
    using size_type = uint;
    struct Iterator
    {
        Iterator() = default;
        Iterator(I const b, I const e) noexcept
            : begin(b)
            , sz(static_cast<size_type>(e - b))
            , v{0u, 0u, 0u}
        {
            if(sz > 0)
                v.idx = begin[0];
            while(++v.second < sz && begin[v.first] == begin[v.second])
                ;
        }

        struct value_type
        {
            size_type idx;
            size_type first;
            size_type second;
        };
        using reference = value_type const &;
        using pointer   = value_type const *;
        using difference_type = std::ptrdiff_t;

        Iterator &operator++() noexcept
        {
            v.first = v.second;
            if(v.first < sz)
                v.idx = begin[v.first];
            while(++v.second < sz && begin[v.first] == begin[v.second])
                ;
            return *this;
        }
        Iterator operator++(int) const noexcept
        {
            Iterator it = *this;
            return ++it;
        }
        reference operator*() const noexcept
        {
            return v;
        }
        pointer operator->() const noexcept
        {
            return &v;
        }
        bool operator!=(Iterator const &other) const noexcept
        {
            return begin + v.first != other.begin + other.v.first;
        }
        bool operator==(Iterator const &other) const noexcept
        {
            return !operator!=(other);
        }

    private:
        I begin;
        size_type sz;
        value_type v;
    };
    static_assert(std::input_or_output_iterator<Iterator>);
    Iterator it0, it1;

    Iterator begin () const noexcept {return it0;}
    Iterator end   () const noexcept {return it1;}
};

template<std::unsigned_integral U = std::size_t, std::ranges::random_access_range R>
auto index_range_sequence(R &&range) noexcept
    -> IndexRangeSequence<U, std::ranges::iterator_t<R>>
{
    auto const begin = std::ranges::begin(range);
    auto const end   = std::ranges::end  (range);
    return
    {
        {}, // std::view_base
        {begin, end},
        {end  , end},
    };
}

} // namespace dsmc::utils
