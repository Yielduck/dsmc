#pragma once
#include <dim/matrix.h>
#include <dsmc/cloud.h>
namespace dsmc::utils
{

struct CloudInfo
{
    dim::scalar<dim::Concentration> n;
    dim::scalar<dim::Density> rho;
    dim::vector<dim::Velocity> u;
    dim::matrix<dim::Pressure> P;
    dim::scalar<dim::Pressure> p;
    dim::scalar<dim::Temperature> T;
    dim::vector<dim::EnergyFlux> q;
};
template<dsmc::concepts::Substance S>
inline CloudInfo integrate(dsmc::Cloud<S> const &cloud, dim::scalar<dim::Concentration> const weight) noexcept
    // weight = molecularStatWeight / cloudVolume
{
    dim::float_t const N = static_cast<dim::float_t>(cloud.size());

    dim::vector<dim::Velocity> u = {0, 0, 0};
    for(auto const &[r, v] : cloud)
        u += v;
    u /= N;

    dim::matrix<dim::pow<2>(dim::Velocity)> c2 =
    {
        {0, 0, 0},
        {0, 0, 0},
        {0, 0, 0},
    };
    dim::vector<dim::pow<3>(dim::Velocity)> c3 = {0, 0, 0};
    for(auto const &[r, v] : cloud)
    {
        dim::vector<dim::Velocity> const c = v - u;
        c2 += dim::outer(c, c);
        c3 += dim::dot(c, c) * c;
    }

    dim::matrix<dim::Pressure> const P = (weight * S::molecularMass) * c2;
    dim::scalar<dim::Pressure> const p = (P[0][0] + P[1][1] + P[2][2]) / 3;

    return
    {
        .n = weight * N,
        .rho = weight * N * S::molecularMass,
        .u = u,
        .P = P,
        .p = p,
        .T = p / (weight * N) / dim::constant::k_B,
        .q = c3 * (weight * S::molecularMass / 2),
    };
}

} // namespace dsmc::utils
