#pragma once
#include <dim/matrix.h>
namespace dsmc::utils
{

inline dim::matrix<dim::None> to_normal_space(dim::vector<dim::None> const &n) noexcept
{
    dim::vector<dim::None> const y0 = dim::abs(n.y) < 0.9
        ? dim::vector<dim::None>(0, 1, 0)
        : dim::vector<dim::None>(1, 0, 0);
    dim::vector<dim::None> const x = dim::normalize(dim::cross(y0, n));
    return dim::transpose(dim::matrix<dim::None>{x, dim::cross(n, x), n});
}

} // namespace dsmc::utils
