#pragma once
#include <dsmc/mixture.h>
namespace dsmc::utils
{

namespace detail
{

template<template<typename...> typename T, typename M>
struct substitute_mixture_impl
{};

template<template<typename...> typename T, dsmc::concepts::Substance... Ss>
struct substitute_mixture_impl<T, dsmc::Mixture<Ss...>>
{
    using type = T<Ss...>;
};

} // namespace detail

template<template<typename...> typename T, typename M>
using substitute_mixture = typename detail::substitute_mixture_impl<T, M>::type;

} // namespace dsmc::utils
