#pragma once
#include <dim/scalar.h>
#include <squares/generator.h>
#include <random>
namespace dsmc::utils
{

using RNG = squares::Generator<3u>;

inline std::uint64_t seed() noexcept
{
    return std::random_device{}();
}

inline thread_local RNG rng = {seed()};

inline dim::float_t canonical(dsmc::utils::RNG &generator = rng) noexcept
    // -> [0, 1)
{
    return std::generate_canonical<dim::float_t, 32>(generator);
}

} // namespace dsmc::utils
