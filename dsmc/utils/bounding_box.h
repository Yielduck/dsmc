#pragma once
#include <dim/vector.h>
namespace dsmc::utils
{

struct BoundingBox
{
    dim::vector<dim::Length> min;
    dim::vector<dim::Length> max;
};

} // namespace dsmc::utils
