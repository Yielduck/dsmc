#pragma once
#include <vector>
#include <queue>
#include <thread>
#include <condition_variable>
#include <mutex>
#include <functional>
namespace dsmc::utils
{

class ThreadPool
{
    bool suspend;
    unsigned int sleeping;
    std::vector<std::thread> thread;
    std::queue<std::function<void()>> taskQueue;
    std::mutex mtx;
    std::condition_variable sleep_cv;
    std::condition_variable wait_cv;

public:
    ThreadPool(unsigned int = std::thread::hardware_concurrency()) noexcept;
    ~ThreadPool();

    template<typename Task>
    void execute(Task &&) noexcept;
    void wait() noexcept;

    auto threadCount() const noexcept {return thread.size();}
};

inline ThreadPool::ThreadPool(unsigned int const count) noexcept
    : suspend(false)
    , sleeping(0u)
{
    for([[maybe_unused]] unsigned int i = 0; i < count; ++i)
        thread.emplace_back
        (
            [this, count]() noexcept
                -> void
            {
                std::function<void()> task;
                while(true)
                {
                    {
                        std::unique_lock<std::mutex> lock(mtx);
                        if(suspend)
                            return;
                        if(taskQueue.empty())
                        {
                            if(++sleeping == count)
                                wait_cv.notify_all();  // if wait()-ing, then notify
                            sleep_cv.wait(lock);
                            --sleeping;
                            continue;
                        }
                        task = std::move(taskQueue.front());
                        taskQueue.pop();
                    }
                    task();
                }
            }
        );
}
inline ThreadPool::~ThreadPool()
{
    {
        std::unique_lock<std::mutex> lock(mtx);
        while(taskQueue.size() != 0u || sleeping != thread.size())
            wait_cv.wait(lock);

        suspend = true;
        sleep_cv.notify_all();
    }
    for(auto &t : thread)
        t.join();
}
template<typename Task>
void ThreadPool::execute(Task &&task) noexcept
{
    std::lock_guard<std::mutex> lock(mtx);
    taskQueue.push(std::forward<Task>(task));
    sleep_cv.notify_one();
}
inline void ThreadPool::wait() noexcept
{
    std::unique_lock<std::mutex> lock(mtx);
    while(taskQueue.size() != 0u || sleeping != thread.size())
        wait_cv.wait(lock);
}

} // namespace dsmc::utils
