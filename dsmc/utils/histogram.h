#pragma once
#include <dim/scalar.h>
#include <dsmc/concepts/random_range_of.h>
#include <algorithm>
#include <vector>
namespace dsmc::utils
{

template<dim::quantity Q, dsmc::concepts::random_range_of<dim::scalar<Q>> R>
auto histogram(R &&f, dim::scalar<Q> const x0, dim::scalar<Q> const x1, unsigned int const B = 200) noexcept
{
    std::vector<unsigned int> H(B, 0);

    for(auto const &x : f)
        ++H[std::clamp(dim::cast<unsigned int>(B * (x - x0) / (x1 - x0)), 0u, B - 1u)];

    unsigned int const N = static_cast<unsigned int>(std::ranges::size(f));
    return std::views::iota(0u, B) | std::views::transform
    (
        [=, hist = std::move(H)](unsigned int const i) noexcept
            -> std::pair
            <
                std::pair<dim::scalar<Q>, dim::scalar<Q>>,
                dim::scalar<dim::inv(Q)>
            >
        {
            return
            {
                {
                    x0 +  i       * (x1 - x0) / B, 
                    x0 + (i + 1u) * (x1 - x0) / B, 
                },
                B * hist[i] / (x1 - x0) / N,
            };
        }
    );
}

} // namespace dsmc::utils
