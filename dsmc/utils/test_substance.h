#pragma once
#include <dsmc/concepts/substance.h>
namespace dsmc::utils
{

struct TestSubstance
{
    static constexpr dim::scalar<dim::Mass> molecularMass = 0.;
};
static_assert(dsmc::concepts::Substance<TestSubstance>);

} // namespace dsmc::utils
