#pragma once
#include <dim/vector.h>
#include <dsmc/utils/rng.h>
#include <numbers>
#include <random>
namespace dsmc::utils
{

inline dim::vector<dim::None> uniform_vec3(dsmc::utils::RNG &generator) noexcept
{
    dim::float_t const cos_theta = dsmc::utils::canonical(generator) * 2 - 1;
    dim::float_t const sin_theta = std::sqrt(1 - cos_theta * cos_theta);
    dim::float_t const phi = dsmc::utils::canonical(generator) * 2 * std::numbers::pi_v<dim::float_t>;
    return
    {
        sin_theta * std::cos(phi),
        sin_theta * std::sin(phi),
        cos_theta,
    };
}

} // namespace dsmc::utils
