#pragma once
#include <dsmc/utils/spawner.h>
#include <ranges>
namespace dsmc::utils
{

template
<
    dsmc::concepts::Substance S,
    dsmc::concepts::PositionDistribution R,
    dsmc::concepts::VelocityDistribution U
>
dsmc::Cloud<S> generateCloud
(
    R positionD,
    U velocityD,
    std::size_t const particleCount,
    dsmc::utils::RNG &generator = dsmc::utils::rng
) noexcept
{
    auto cloud = std::views::iota(0ull, particleCount) | std::views::transform
    (
        dsmc::utils::uspawner<S>
        (
            std::move(positionD),
            std::move(velocityD),
            generator
        )
    ) | std::views::common;
    return
    {
        std::ranges::begin(cloud),
        std::ranges::  end(cloud),
    };
}

} // namespace dsmc::utils
