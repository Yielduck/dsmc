#pragma once
#include <dsmc/concepts/substance.h>
#include <dsmc/concepts/position_distribution.h>
#include <dsmc/concepts/velocity_distribution.h>
#include <dsmc/utils/rng.h>
namespace dsmc::utils
{

template
<
    dsmc::concepts::Substance S,
    dsmc::concepts::PositionDistribution R,
    dsmc::concepts::VelocityDistribution V
>
auto spawner(R positionD, V velocityD, dsmc::utils::RNG &generator = dsmc::utils::rng) noexcept
{
    return [&generator, r = std::move(positionD), u = std::move(velocityD)]() mutable noexcept
        -> dsmc::Particle<S>
    {
        return
        {
            r(generator),
            u(generator),
        };
    };
}
template
<
    dsmc::concepts::Substance S,
    dsmc::concepts::PositionDistribution R,
    dsmc::concepts::VelocityDistribution V
>
auto uspawner(R positionD, V velocityD, dsmc::utils::RNG &generator = dsmc::utils::rng) noexcept
{
    return [&generator, r = std::move(positionD), u = std::move(velocityD)](std::size_t) mutable noexcept
        -> dsmc::Particle<S>
    {
        return
        {
            r(generator),
            u(generator),
        };
    };
}

} // namespace dsmc::utils
