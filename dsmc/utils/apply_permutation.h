#pragma once
#include <ranges>
namespace dsmc::utils
{

template<std::ranges::random_access_range R1, std::ranges::random_access_range R2>
void apply_permutation(R1 &&target, R2 &&index) noexcept
{
    for(auto const i : std::views::iota(0u, std::ranges::size(index)))
    {
        auto j = i;
        while(i != index[i])
        {
            auto const k = index[i];
            std::swap(target[j], target[k]);
            std::swap( index[i],  index[k]);
            j = k;
        }
    }
}

} // namespace dsmc::utils
