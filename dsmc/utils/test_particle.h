#pragma once
#include <dsmc/particle.h>
#include <dsmc/utils/test_substance.h>
namespace dsmc::utils
{

using TestParticle = dsmc::Particle<dsmc::utils::TestSubstance>;

} // namespace dsmc::utils
