#pragma once
#include <dim/vector.h>
#include <dsmc/concepts/substance.h>
namespace dsmc
{

template<dsmc::concepts::Substance S>
struct Particle
{
    using Substance = S;

    dim::vector<dim::Length  > r;
    dim::vector<dim::Velocity> v;
};

} // namespace dsmc
