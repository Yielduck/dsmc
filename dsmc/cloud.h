#pragma once
#include <dsmc/particle.h>
#include <vector>
namespace dsmc
{

template<dsmc::concepts::Substance S>
using Cloud = std::vector
<
    dsmc::Particle<S>
>;

} // namespace dsmc
