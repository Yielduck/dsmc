#pragma once
#include <dsmc/cell.h>
#include <dsmc/mixture.h>
#include <dsmc/concepts/collision_model.h>
#include <dsmc/utils/rng.h>
#include <array>
namespace dsmc
{

template
<
    dsmc::concepts::CollisionModel CollisionModel,
    dsmc::concepts::Substance... Ss
>
auto collide
(
    dsmc::Cell<Ss...> &cell,
    dim::scalar<dim::Time> const t0,
    dsmc::utils::RNG &generator = dsmc::utils::rng
) noexcept
    requires(... && (CollisionModel::Table::Mixture::template indexOf<Ss> != pp::not_found))
{
    using Mixture = dsmc::Mixture<Ss...>;
    std::array<std::size_t, Mixture::substanceCount> collisionCount = {};

    cell.template forEachCloud
    (
        [&]<dsmc::concepts::Substance S1>(dsmc::Cloud<S1> &cloud1) noexcept
            -> void
        {
            cell.template forEachCloud
            (
                [&]<dsmc::concepts::Substance S2>(dsmc::Cloud<S2> &cloud2) noexcept
                    -> void
                {
                    constexpr pp::type_index I1 = Mixture::template indexOf<S1>;
                    constexpr pp::type_index I2 = Mixture::template indexOf<S2>;
                    if(I1 > I2)
                        return;

                    dim::scalar<dim::None> const N1 = static_cast<dim::float_t>(cloud1.size());
                    dim::scalar<dim::None> const N2 = static_cast<dim::float_t>(cloud2.size());
                    dim::scalar<dim::None> const pairs = I1 == I2
                        ? N1 * (N1 - 1) / 2
                        : N1 * N2;
                    if(pairs == 0)
                        return;

#define MCF

#ifdef BT
                    for(std::size_t i = 0; i < cloud1.size(); ++i)
                        for(std::size_t j = i + 1; j < cloud2.size(); ++j)
                        {
                            dsmc::Particle<S1> &p1 = cloud1[i];
                            dsmc::Particle<S2> &p2 = cloud2[j];
                            dim::scalar<dim::mul(dim::Area, dim::Velocity)> const sigmaVr
                                = dim::length(p1.v - p2.v)
                                * CollisionModel::template crossSection(p1, p2);
                            if(cell.weight * sigmaVr * t0 < dsmc::utils::canonical(generator))
                                continue;

                            CollisionModel::template collide(p1, p2);

                            if constexpr(I1 == I2)
                                collisionCount[I1] += 2;
                            else
                            {
                                ++collisionCount[I1];
                                ++collisionCount[I2];
                            }
                        }
#endif
#ifdef CF
                    std::uniform_int_distribution<std::size_t> i1D(0u, cloud1.size() - 1u);
                    std::uniform_int_distribution<std::size_t> i2D(0u, cloud2.size() - 1u);

                    dim::scalar<dim::Time> t = 0.;
                    while(true)
                    {
                        std::size_t const i1 = i1D(generator);
                        std::size_t const i2 = i2D(generator);
                        if constexpr(I1 == I2)
                            if(i1 == i2)
                                continue;

                        dim::scalar<dim::Frequency> nuSum = 0;
                        for(std::size_t i = 0; i < cloud1.size(); ++i)
                            for(std::size_t j = i + 1; j < cloud2.size(); ++j)
                            {
                                dsmc::Particle<S1> const &p1 = cloud1[i];
                                dsmc::Particle<S2> const &p2 = cloud2[j];
                                dim::scalar<dim::mul(dim::Area, dim::Velocity)> const sigmaVr
                                    = dim::length(p1.v - p2.v)
                                    * CollisionModel::template crossSection(p1, p2);
                                nuSum += cell.weight * sigmaVr;
                            }
                        t += std::exponential_distribution<dim::float_t>{nuSum.value}(generator);
                        if(t >= t0)
                            break;

                        CollisionModel::template collide(cloud1[i1], cloud2[i2]);

                        if constexpr(I1 == I2)
                            collisionCount[I1] += 2;
                        else
                        {
                            ++collisionCount[I1];
                            ++collisionCount[I2];
                        }
                    }
#endif
#ifdef MCF
                    auto indexPair =
                    [
                        i1D = std::uniform_int_distribution{0ull, cloud1.size() - 1ull},
                        i2D = std::uniform_int_distribution{0ull, cloud2.size() - 1ull}
                    ](dsmc::utils::RNG &g) mutable noexcept
                        -> std::pair<std::size_t, std::size_t>
                    {
                        if constexpr(I1 != I2)
                            return {i1D(g), i2D(g)};
                        while(true)
                        {
                            std::size_t const i1 = i1D(g);
                            std::size_t const i2 = i2D(g);
                            if(i1 != i2)
                                return {i1, i2};
                        }
                    };
                    auto const sigmaVr = [&](std::pair<std::size_t, std::size_t> const &i) noexcept
                    {
                        dsmc::Particle<S1> const &p1 = cloud1[i.first];
                        dsmc::Particle<S2> const &p2 = cloud2[i.second];
                        return dim::length(p1.v - p2.v)
                             * CollisionModel::template crossSection(p1, p2);
                    };

                    dim::scalar<dim::mul(dim::Area, dim::Velocity)> &sigmaVrMax = cell.template sigmaVrMax<S1, S2>();
                    sigmaVrMax = std::max(sigmaVrMax, sigmaVr(indexPair(generator)));

                    std::exponential_distribution<dim::float_t> dtD((pairs * cell.weight * sigmaVrMax).value);

                    dim::scalar<dim::Time> t = 0.;
                    while((t += dtD(generator)) < t0)
                    {
                        auto const [i1, i2] = indexPair(generator);

                        auto const sigmaVr12 = sigmaVr({i1, i2});
                        if(sigmaVr12 > sigmaVrMax)
                        {
                            sigmaVrMax = sigmaVr12;
                            dtD = std::exponential_distribution<dim::float_t>{(pairs * cell.weight * sigmaVrMax).value};
                        }
                        else if(sigmaVr12 < sigmaVrMax * dsmc::utils::canonical(generator))
                            continue;

                        CollisionModel::template collide(cloud1[i1], cloud2[i2]);

                        if constexpr(I1 == I2)
                            collisionCount[I1] += 2;
                        else
                        {
                            ++collisionCount[I1];
                            ++collisionCount[I2];
                        }
                    }
#endif
                }
            );
        }
    );
    return collisionCount;
}

} // namespace dsmc
