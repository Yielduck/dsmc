#pragma once
#include <dsmc/surface/hit.h>
#include <dsmc/utils/rng.h>
#include <concepts>
#include <optional>
namespace dsmc::concepts
{

template<typename T>
concept Surface = requires
(
    T const surface,
    dim::vector<dim::Length> const r,
    dim::vector<dim::Velocity> const v
)
{
    {surface.point(dsmc::utils::rng)} -> std::same_as<dim::vector<dim::Length>>;
    {surface.hit(r, v)} -> std::same_as<std::optional<dsmc::surface::Hit>>;
    {surface.area()} -> std::same_as<dim::scalar<dim::Area>>;
    {surface.norm()} -> std::same_as<dim::vector<dim::None>>;
};

} // namespace dsmc::concepts
