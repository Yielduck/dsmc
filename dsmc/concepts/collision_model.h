#pragma once
#include <dsmc/particle.h>
namespace dsmc::concepts
{

template<typename T>
concept CollisionModel = requires
(
    typename T::Table::Mixture::template Substance<0> S,
    dsmc::Particle<decltype(S)>       &pRef,
    dsmc::Particle<decltype(S)> const &pCRef
)
{
    {T::collide(pRef, pRef)} -> std::same_as<void>;
    {T::crossSection(pCRef, pCRef)} -> std::same_as<dim::scalar<dim::Area>>;
};

} // namespace dsmc::concepts
