#pragma once
#include <concepts>
#include <dim/scalar.h>
namespace dsmc::concepts
{

template<typename S>
concept Substance = requires()
{
    {S::molecularMass} -> std::convertible_to<dim::scalar<dim::Mass>>;
};

} // namespace dsmc::concepts
