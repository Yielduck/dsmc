#pragma once
#include <dim/vector.h>
#include <dsmc/utils/rng.h>
#include <concepts>
namespace dsmc::concepts
{

template<typename T>
concept PositionDistribution = requires(T distribution, dsmc::utils::RNG &generator)
{
    {distribution(generator)} -> std::same_as<dim::vector<dim::Length>>;
};

} // namespace dsmc::concepts
