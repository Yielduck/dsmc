#pragma once
#include <dsmc/utils/bounding_box.h>
namespace dsmc::concepts
{

template<typename T>
concept Mesh = requires
(
    T const mesh,
    typename T::idx_t cellID,
    dim::vector<dim::Length> r
)
{
    {mesh.cells         ()      } -> std::same_as<typename T::idx_t>;
    {mesh.pointCell     (r)     } -> std::same_as<typename T::idx_t>;
    {mesh.cellVolume    (cellID)} -> std::same_as<dim::scalar<dim::Volume>>;
    {mesh.boundingBox   (cellID)} -> std::same_as<dsmc::utils::BoundingBox>;
};

} // namespace dsmc::concepts
