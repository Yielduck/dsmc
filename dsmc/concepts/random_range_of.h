#pragma once
#include <dsmc/particle.h>
#include <ranges>
namespace dsmc::concepts
{

template<typename R, typename T>
concept random_range_of = std::ranges::random_access_range<R>
                       && std::is_same_v<T, std::ranges::range_value_t<R>>;

} // namespace dsmc::concepts
