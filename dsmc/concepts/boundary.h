#pragma once
#include <dsmc/boundary/hit.h>
#include <dsmc/surface/hit.h>
#include <dsmc/concepts/random_range_of.h>
#include <dsmc/utils/test_particle.h>
#include <concepts>
#include <optional>
namespace dsmc::concepts
{

template<typename T>
concept Boundary = requires
(
    dsmc::utils::TestParticle const p,
    dim::scalar<dim::Time> const dt,
    T boundary,
    T const constBoundary
)
{
    {constBoundary.surfaceHit(p.r, p.v)}
        -> std::same_as<std::optional<dsmc::surface::Hit>>;
    {boundary.hit(p)}
        -> std::same_as<dsmc::boundary::Hit>;
    {boundary.template spawn<dsmc::utils::TestSubstance>(dt)}
        -> dsmc::concepts::random_range_of<dsmc::Particle<dsmc::utils::TestSubstance>>;
};

} // namespace dsmc::concepts
