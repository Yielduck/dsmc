#pragma once
#include <dsmc/particle.h>
#include <dsmc/concepts/mesh.h>
#include <dsmc/utils/apply_permutation.h>
#include <dsmc/utils/index_range_sequence.h>
#include <algorithm>
#include <vector>
namespace dsmc
{

template
<
    std::ranges::random_access_range R,
    dsmc::concepts::Mesh Mesh
>
auto sort(R &&cloud, Mesh const &mesh) noexcept
    requires std::ranges::output_range
    <
        R,
        dsmc::Particle
        <
            typename std::ranges::range_value_t<R>::Substance
        >
    >
   // -> view<pair<cellID, particleRange>>
   // cloud is forwarded into the return value
{
    using idx_t = Mesh::idx_t;
    static thread_local std::vector<std::pair<idx_t, idx_t>> info;

    auto const infoR = std::views::iota(0u, static_cast<idx_t>(std::ranges::size(cloud))) | std::views::transform
    (
        [&](idx_t const i) noexcept
            -> std::pair<idx_t, idx_t>
        {
            return
            {
                mesh.pointCell(cloud[i].r),
                i,
            };
        }
    ) | std::views::common;
    info.assign(std::ranges::cbegin(infoR), std::ranges::cend(infoR));

    std::ranges::sort(info, {}, &std::pair<idx_t, idx_t>::first);
    dsmc::utils::apply_permutation(cloud, info | std::views::values);

    return dsmc::utils::index_range_sequence<idx_t>(info | std::views::keys) | std::views::transform
    (
        [range = std::forward<R>(cloud)](auto const &I) noexcept
        {
            auto const &[cellID, begin, end] = I;
            return std::pair
            {
                cellID,
                range | std::views::take(end)
                      | std::views::drop(begin)
            };
        }
    );
}

} // namespace dsmc
