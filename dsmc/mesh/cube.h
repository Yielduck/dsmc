#pragma once
#include <dsmc/concepts/mesh.h>
namespace dsmc::mesh
{

struct Cube
{
    using idx_t = unsigned int;
    idx_t dim[3];

    dim::vector<dim::Length> min;
    dim::vector<dim::Length> step;

    idx_t cellCount;
    dim::scalar<dim::Volume> V;

    Cube() noexcept
        : dim{1u, 1u, 1u}
        , min({0., 0., 0.})
        , step({1., 1., 1.})
        , cellCount(1u)
        , V(1.)
    {}
    Cube(dsmc::utils::BoundingBox const &bbox, idx_t const d[3]) noexcept
        : dim{d[0], d[1], d[2]}
        , min(bbox.min)
        , step((bbox.max - bbox.min) / dim::vector<dim::None>{d[0], d[1], d[2]})
        , cellCount(d[0] * d[1] * d[2])
        , V(step.x * step.y * step.z)
    {}
    idx_t cells() const noexcept
    {
        return cellCount;
    }
    dsmc::utils::BoundingBox boundingBox(idx_t const cellID) const noexcept
    {
        assert(cellID < cells());

        idx_t const sxy = dim[0] * dim[1];
        idx_t const x =  (cellID % sxy) % dim[0];
        idx_t const y = ((cellID % sxy) / dim[0]) % dim[1];
        idx_t const z =   cellID / sxy;
        dim::vector<dim::Length> const p0 = min + step * dim::vector<dim::None>{x, y, z};
        return
        {
            p0,
            p0 + step,
        };
    }
    idx_t pointCell(dim::vector<dim::Length> r) const noexcept
    {
        dim::vector<dim::None> const I = (r - min) / step;
        auto const clamp = [](dim::scalar<dim::None> const s, idx_t const d) noexcept
            -> idx_t
        {
            idx_t const is = dim::cast<idx_t>(s);
            return s.value < 0
                ? 0u
                : (is > d ? d : is);
        };
        return clamp(I.x, dim[0] - 1)
             + clamp(I.y, dim[1] - 1) * dim[0]
             + clamp(I.z, dim[2] - 1) * dim[0] * dim[1];
    }
    dim::scalar<dim::Volume> cellVolume(idx_t) const noexcept
    {
        return V;
    }
};

static_assert(dsmc::concepts::Mesh<Cube>);

} // namespace dsmc::mesh
