#pragma once
#include <dim/constant.h>
#include <dim/vector.h>
#include <dsmc/concepts/substance.h>
#include <dsmc/utils/rng.h>
namespace dsmc::distribution
{

template<dsmc::concepts::Substance S>
struct Maxwell
{
    dim::vector<dim::Velocity> mean;
    std::normal_distribution<dim::float_t> distribution;

    Maxwell
    (
        dim::scalar<dim::Temperature> const T,
        dim::vector<dim::Velocity> const &U = {0., 0., 0.}
    ) noexcept
        : mean(U)
        , distribution(0., dim::sqrt(dim::constant::k_B * T / S::molecularMass).value)
    {}
    dim::vector<dim::Velocity> operator()(dsmc::utils::RNG &generator) noexcept
    {
        return mean + dim::vector<dim::Velocity>
        {
            distribution(generator),
            distribution(generator),
            distribution(generator),
        };
    }
};

} // namespace dsmc::distribution
