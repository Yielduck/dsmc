#pragma once
#include <dim/scalar.h>
#include <random>
#include <cmath>
namespace dsmc::distribution
{

struct Interval : public std::uniform_real_distribution<dim::float_t>
{
    template<dim::quantity Q>
    Interval(dim::scalar<Q> const min, dim::scalar<Q> const max) noexcept
        : std::uniform_real_distribution<dim::float_t>
            (
                std::nextafter(min.value, std::numeric_limits<dim::float_t>::max()),
                max.value
            )
    {}
};

} // namespace dsmc::distribution
