#pragma once
#include <dim/constant.h>
#include <dsmc/concepts/substance.h>
#include <dsmc/concepts/velocity_distribution.h>
#include <dsmc/utils/rng.h>
#include <dsmc/utils/to_normal_space.h>
#include <numbers>
namespace dsmc::distribution
{

template<dsmc::concepts::Substance S>
struct Inflow
{
    dim::vector<dim::Velocity> mean;
    dim::scalar<dim::pow<2>(dim::Velocity)> variance;
    dim::scalar<dim::Velocity> vT;
    dim::scalar<dim::None> a;
    std::normal_distribution<dim::float_t> nD;
    dim::matrix<dim::None> M;

    Inflow
    (
        dim::vector<dim::None> const &n,
        dim::scalar<dim::Temperature> const T,
        dim::vector<dim::Velocity> const &U = {0, 0, 0}
    ) noexcept
        : mean(U)
        , variance(dim::constant::k_B * T / S::molecularMass)
        , vT(dim::sqrt(2 * variance))
        , a(dim::dot(n, U) / vT)
        , nD(0., dim::sqrt(variance).value)
        , M(dsmc::utils::to_normal_space(n))
    {}
    dim::vector<dim::Velocity> operator()(dsmc::utils::RNG &generator) noexcept
    {
        auto const vz = [this](dsmc::utils::RNG &gen) noexcept
            -> dim::scalar<dim::Velocity>
        {
            if(a > 0)
            {
                auto const b = a * std::sqrt(std::numbers::pi_v<dim::float_t>);
                auto const p0 = 1 / (1 + 2 * b);
                while(true)
                {
                    auto const p = dsmc::utils::canonical(gen);
                    dim::scalar<dim::Velocity> const v = p < p0
                        ? vT * std::sqrt(-std::log(dsmc::utils::canonical(gen)))
                        : nD(gen);
                    if(a * vT + v > a * vT * dsmc::utils::canonical(gen))
                        return v;
                }
            }
            else
                while(true)
                {
                    dim::scalar<dim::None> const z = dim::sqrt(a * a - std::log(dsmc::utils::canonical(gen)));
                    if(a + z > dsmc::utils::canonical(gen) * z)
                        return vT * z;
                }
        };
        dim::vector<dim::Velocity> const v =
        {
            nD(generator),
            nD(generator),
            vz(generator),
        };
        return mean + M * v;
    }
};

} // namespace dsmc::distribution
