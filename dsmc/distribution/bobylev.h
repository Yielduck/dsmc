#pragma once
#include <dim/constant.h>
#include <dsmc/utils/rng.h>
namespace dsmc::distribution
{

template<dsmc::concepts::Substance S>
struct Bobylev
{
    dim::float_t const a;
    dim::float_t const b;

    Bobylev
    (
        dim::scalar<dim::Temperature> const T,
        dim::scalar<dim::None> const beta
    ) noexcept
        : a(dim::sqrt((dim::scalar<dim::None>(1.) + beta) * S::molecularMass / dim::constant::k_B / T).value)
        , b(beta.value)
    {}
    dim::scalar<dim::Velocity> pdf(dim::float_t const v) const noexcept
    {
        return a / std::sqrt(2 * std::numbers::pi_v<dim::float_t>)
             * std::exp(-0.5 * a * a * v * v)
             * (1. + b * 0.5 * (a * a * v * v - 1.));
    }
    dim::vector<dim::Velocity> operator()(dsmc::utils::RNG &generator) noexcept
    {
        dim::vector<dim::Velocity> u;
        std::uniform_real_distribution<dim::float_t> D(-5 / a, 5 / a);
        for(unsigned int i = 0; i < 3; ++i)
        {
            while(true)
            {
                dim::float_t const x = dsmc::utils::canonical(generator);
                dim::float_t const v = D(generator);
                if(x * pdf(0.) < pdf(v))
                {
                    u[i] = v;
                    break;
                }
            }
        }
        return u;
    }
};

} // namespace dsmc::distribution
