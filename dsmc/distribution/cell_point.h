#pragma once
#include <dsmc/concepts/mesh.h>
#include <dsmc/distribution/bounding_box.h>
namespace dsmc::distribution
{

template<dsmc::concepts::Mesh Mesh>
class CellPoint
{
    Mesh const &mesh;
    typename Mesh::idx_t cellID;
    dsmc::distribution::BoundingBox D;

public:
    CellPoint(Mesh const &m, typename Mesh::idx_t const cell) noexcept
        : mesh(m)
        , cellID(cell)
        , D(mesh.boundingBox(cellID))
    {}
    dim::vector<dim::Length> operator()(dsmc::utils::RNG &generator) noexcept
    {
        while(true)
        {
            dim::vector<dim::Length> const r = D(generator);
            if(mesh.pointCell(r) == cellID)
                return r;
        }
    }
};

} // namespace dsmc::distribution
