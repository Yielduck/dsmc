#pragma once
#include <dim/constant.h>
#include <dsmc/concepts/substance.h>
#include <dsmc/concepts/velocity_distribution.h>
#include <dsmc/utils/rng.h>
#include <dsmc/utils/to_normal_space.h>
namespace dsmc::distribution
{

template<dsmc::concepts::Substance S>
struct Diffuse
{
    dim::vector<dim::Velocity> mean;
    dim::scalar<dim::pow<2>(dim::Velocity)> variance;
    std::normal_distribution<dim::float_t> nD;
    std::chi_squared_distribution<dim::float_t> rD;
    dim::matrix<dim::None> M;

    Diffuse
    (
        dim::vector<dim::None> const &n,
        dim::scalar<dim::Temperature> const T,
        dim::vector<dim::Velocity> const &U = {0, 0, 0}
    ) noexcept
        : mean(U)
        , variance(dim::constant::k_B * T / S::molecularMass)
        , nD(0., dim::sqrt(variance).value)
        , rD(2)
        , M(dsmc::utils::to_normal_space(n))
    {}
    dim::vector<dim::Velocity> operator()(dsmc::utils::RNG &generator) noexcept
    {
        dim::vector<dim::Velocity> const v =
        {
            nD(generator),
            nD(generator),
            dim::sqrt(variance * rD(generator))
        };
        return mean + M * v;
    }
};

} // namespace dsmc::distribution
