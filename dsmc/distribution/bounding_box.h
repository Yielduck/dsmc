#pragma once
#include <dsmc/concepts/position_distribution.h>
#include <dsmc/distribution/interval.h>
#include <dsmc/utils/bounding_box.h>
#include <dsmc/utils/rng.h>
namespace dsmc::distribution
{

struct BoundingBox
{
    dsmc::distribution::Interval x, y, z;
    BoundingBox(dsmc::utils::BoundingBox const &bbox) noexcept
        : x(bbox.min.x, bbox.max.x)
        , y(bbox.min.y, bbox.max.y)
        , z(bbox.min.z, bbox.max.z)
    {}
    dim::vector<dim::Length> operator()(dsmc::utils::RNG &generator) noexcept
    {
        return
        {
            x(generator),
            y(generator),
            z(generator),
        };
    }
};

static_assert(dsmc::concepts::PositionDistribution<BoundingBox>);

} // namespace dsmc::distribution
