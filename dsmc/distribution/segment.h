#pragma once
#include <dim/scalar.h>
#include <random>
#include <cmath>
namespace dsmc::distribution
{

struct Segment : public std::uniform_real_distribution<dim::float_t>
{
    template<dim::quantity Q>
    Segment(dim::scalar<Q> const min, dim::scalar<Q> const max) noexcept
        : std::uniform_real_distribution<dim::float_t>
            (
                min.value,
                std::nextafter(max.value, std::numeric_limits<dim::float_t>::max())
            )
    {}
};

} // namespace dsmc::distribution
