#pragma once
#include <dsmc/concepts/substance.h>
#include <pp/find_type.h>
#include <pp/pick_type.h>
namespace dsmc
{

template<dsmc::concepts::Substance... Ss>
struct Mixture
{
    static constexpr pp::type_index substanceCount = sizeof...(Ss);

    template<pp::type_index I>
    using Substance = typename pp::pick_type<I>::template from<Ss...>;

    template<dsmc::concepts::Substance S>
    static constexpr pp::type_index indexOf = pp::find_type<S>::template in<Ss...>;

    template<typename Visitor>
    static void forEachSubstance(Visitor &&f) noexcept
    {
        (..., f.template operator()<Ss>());
    }
};

} // namespace dsmc
