#include <dsmc/cloud.h>
#include <dsmc/sort.h>
#include <dsmc/mesh/cube.h>
#include <dsmc/distribution/bounding_box.h>
#include <dsmc/utils/index_range_sequence.h>
#include <dsmc/utils/test_substance.h>
#include <gtest/gtest.h>

TEST(cube, pointCell)
{
    dsmc::utils::BoundingBox const bbox = {{0., 0., 0.}, {1., 1., 1.}};
    using idx_t = dsmc::mesh::Cube::idx_t;
    idx_t const dim[3] = {5u, 6u, 7u};
    dsmc::mesh::Cube const cube = {bbox, dim};
    for(idx_t const cellID : std::views::iota(0u, cube.cells()))
    {
        dsmc::distribution::BoundingBox D = {cube.boundingBox(cellID)};
        for([[maybe_unused]] auto const i : std::views::iota(0u, 100000u))
            ASSERT_EQ(cellID, cube.pointCell(D(dsmc::utils::rng)));
    }
}
TEST(utils, index_range_sequence)
{
    dsmc::utils::RNG generator(dsmc::utils::seed());
    std::uniform_int_distribution<unsigned int> D(0u, 64u);
    std::vector<unsigned int> v1;
    for(unsigned int const i : std::views::iota(0u, 1000000u))
        for([[maybe_unused]] unsigned int const j : std::views::iota(0u, D(generator)))
            v1.push_back(i);

    std::vector<unsigned int> v2;
    auto const R = dsmc::utils::index_range_sequence<unsigned int>(v1);
    for(auto const &[i, begin, end] : R)
    {
        auto const view = v1 | std::views::take(end)
                             | std::views::drop(begin);
        for(unsigned int const j : view)
            ASSERT_EQ(i, j);

        v2.insert(v2.end(), std::ranges::begin(view), std::ranges::end(view));
    }

    ASSERT_EQ(v1.size(), v2.size());
    for(std::size_t i = 0; i < v1.size(); ++i)
        ASSERT_EQ(v1[i], v2[i]);
}
TEST(basic, sort)
{
    dsmc::utils::RNG generator(dsmc::utils::seed());

    dsmc::utils::BoundingBox const bbox = {{-1., -1., -1.}, {1., 1., 1.}};
    using idx_t = dsmc::mesh::Cube::idx_t;
    idx_t const dim[3] = {10u, 11u, 13u};
    dsmc::mesh::Cube const cube = {bbox, dim};
    dsmc::distribution::BoundingBox D = {bbox};

    using S = dsmc::utils::TestSubstance;
    dsmc::Cloud<S> cloud;
    auto const particleRange = std::views::iota(0u, 1000000u) | std::views::transform
    (
        [&](idx_t) noexcept
            -> dsmc::Particle<S>
        {
            return
            {
                D(generator),
                {0., 0., 0.},
            };
        }
    );
    cloud.assign(std::ranges::begin(particleRange), std::ranges::end(particleRange));
    for(auto const &[cellID, R] : dsmc::sort(std::move(cloud), cube))
        for(auto const &[r, v] : R)
            ASSERT_EQ(cellID, cube.pointCell(r));
}

int main(int argc, char *argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
