data = '5e8'

dt = 5e-8
dx = 0.005

set format y '%e'
set xrange[0:1]
set linetype 8 lc rgb "gray" lw 1
set grid lt 8

mA = 8e-26
mB = 4e-26
mC = 1e-26

dA = 4.16216e-10
dB = 3.49995e-10
dC = 2.47484e-10
k_B = 1.380649e-23
T = 273.15
n_stp = 1e5 / k_B / T

nA(x) = na5 * x ** 5 + na4 * x ** 4 + na3 * x ** 3 + na2 * x ** 2 + na1 * x + na0
fit nA(x) data using 1:2 via na0, na1, na2, na3, na4, na5
dnA(x) = 5 * na5 * x ** 4 + 4 * na4 * x ** 3 + 3 * na3 * x ** 2 + 2 * na2 * x + na1
nB(x) = nb5 * x ** 5 + nb4 * x ** 4 + nb3 * x ** 3 + nb2 * x ** 2 + nb1 * x + nb0
fit nB(x) data using 1:3 via nb0, nb1, nb2, nb3, nb4, nb5
dnB(x) = 5 * nb5 * x ** 4 + 4 * nb4 * x ** 3 + 3 * nb3 * x ** 2 + 2 * nb2 * x + nb1
nC(x) = nc5 * x ** 5 + nc4 * x ** 4 + nc3 * x ** 3 + nc2 * x ** 2 + nc1 * x + nc0
fit nC(x) data using 1:4 via nc0, nc1, nc2, nc3, nc4, nc5
dnC(x) = 5 * nc5 * x ** 4 + 4 * nc4 * x ** 3 + 3 * nc3 * x ** 2 + 2 * nc2 * x + nc1

#plot data using 1:(($2 - nA($1)) / $2) with histeps,\
#     data using 1:(($3 - nB($1)) / $3) with histeps,\
#     data using 1:(($4 - nC($1)) / $4) with histeps

n(x) = nA(x) + nB(x) + nC(x)
dn(x) = dnA(x) + dnB(x) + dnC(x)

xA(x) = nA(x) / n(x)
xB(x) = nB(x) / n(x)
xC(x) = nC(x) / n(x)
dXA(x) = (dnA(x) * n(x) - nA(x) * dn(x)) / (n(x) ** 2)
dXB(x) = (dnB(x) * n(x) - nB(x) * dn(x)) / (n(x) ** 2)
dXC(x) = (dnC(x) * n(x) - nC(x) * dn(x)) / (n(x) ** 2)

jA(x) = jA0
fit jA(x) data using 1:11 via jA0
jB(x) = jB0
fit jB(x) data using 1:12 via jB0
jC(x) = jC0
fit jC(x) data using 1:13 via jC0

rhoA(x) = mA * nA(x)
rhoB(x) = mB * nB(x)
rhoC(x) = mC * nC(x)
drhoA(x) = mA * dnA(x)
drhoB(x) = mB * dnB(x)
drhoC(x) = mC * dnC(x)
rho(x) = rhoA(x) + rhoB(x) + rhoC(x)
drho(x) = drhoA(x) + drhoB(x) + drhoC(x)

cA(x) = rhoA(x) / rho(x)
cB(x) = rhoB(x) / rho(x)
cC(x) = rhoC(x) / rho(x)
dcA(x) = (drhoA(x) * rho(x) - rhoA(x) * drho(x)) / (rho(x) ** 2)
dcB(x) = (drhoB(x) * rho(x) - rhoB(x) * drho(x)) / (rho(x) ** 2)
dcC(x) = (drhoC(x) * rho(x) - rhoC(x) * drho(x)) / (rho(x) ** 2)

uA(x) = jA(x) / rhoA(x)
uB(x) = jB(x) / rhoB(x)
uC(x) = jC(x) / rhoC(x)
u(x) = (jA(x) + jB(x) + jC(x)) / rho(x)

vA(x) = uA(x) - u(x)
vB(x) = uB(x) - u(x)
vC(x) = uC(x) - u(x)

IA(x) = rhoA(x) * vA(x)
IB(x) = rhoB(x) * vB(x)
IC(x) = rhoC(x) * vC(x)

#D0AB(x) = 1.445596e-5 * n_stp / n(x)
#D0AC(x) = 3.337040e-5 * n_stp / n(x)
#D0BC(x) = 4.340490e-5 * n_stp / n(x)

#IA(x) = rho(x) * (D0AB(x) * dcB(x) + D0AC(x) * dcC(x))
#IB(x) = rho(x) * (D0AB(x) * dcA(x) + D0BC(x) * dcC(x))
#IC(x) = rho(x) * (D0AC(x) * dcA(x) + D0BC(x) * dcB(x))

DBC(x) =  0.5 * (-IA(x) * dcA(x) / dcB(x) / dcC(x) + IB(x) / dcC(x) + IC(x) / dcB(x)) / rho(x) * n(x) / n_stp
DAC(x) =  0.5 * (-IB(x) * dcB(x) / dcA(x) / dcC(x) + IA(x) / dcC(x) + IC(x) / dcA(x)) / rho(x) * n(x) / n_stp
DAB(x) =  0.5 * (-IC(x) * dcC(x) / dcA(x) / dcB(x) + IA(x) / dcB(x) + IB(x) / dcA(x)) / rho(x) * n(x) / n_stp

plot DAC(x), DBC(x), DAB(x), 1.445596e-5, 3.337e-5, 4.34e-5

#dxA(x) = xA(x) * xB(x) / DAB(x) * (uB(x) - uA(x)) + xA(x) * xC(x) / DAC(x) * (uC(x) - uA(x))
#plot dxA(x), dXA(x)

vM(m, T) = (8 * k_B * T / pi / m) ** 0.5

dAB = 0.5 * (dA + dB)
dAC = 0.5 * (dA + dC)
dBC = 0.5 * (dB + dC)
nu(dpq, nq, mp, mq) = pi * (dpq ** 2) * nq * ((1 + mp / mq) ** 0.5)
l0(n0, n1, n2) = n0 / (n0 + n1 + n2) / (nu(dA , n0, mA, mA) + nu(dAB, n1, mA, mB) + nu(dAC, n2, mA, mC))\
               + n1 / (n0 + n1 + n2) / (nu(dAB, n0, mB, mA) + nu(dB , n1, mB, mB) + nu(dBC, n2, mB, mC))\
               + n2 / (n0 + n1 + n2) / (nu(dAC, n0, mC, mA) + nu(dBC, n1, mC, mB) + nu(dC , n2, mC, mC))
#plot data using 1:(dx / l0($2, $3, $4)) with histeps
#plot data using 1:(($5 * vM(mA, $14) + $6 * vM(mB, $15) + $7 * vM(mC, $16)) * dt / l0($2, $3, $4)) with histeps
