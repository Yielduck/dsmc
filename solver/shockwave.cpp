#include <iostream>
#include <fstream>

#include "xe_tab.h"

#include <dim/write.h>

#include <dsmc/solver.h>

#include <dsmc/collision_model/variable_soft_sphere.h>
using CollisionModel = dsmc::collision_model::VariableSoftSphere<Table>;

#include <dsmc/boundary/collection.h>
#include <dsmc/boundary/inflow.h>
#include <dsmc/boundary/symmetry.h>
#include <dsmc/surface/xrectangle.h>
using Boundary = dsmc::boundary::Collection
<
    dsmc::boundary::Inflow<dsmc::surface::XRectangle<true >, He, Xe>,
    dsmc::boundary::Inflow<dsmc::surface::XRectangle<false>, He, Xe>,
    dsmc::boundary::Symmetry<1>,
    dsmc::boundary::Symmetry<2>
>;

#include <dsmc/mesh/cube.h>
using Mesh = dsmc::mesh::Cube;

#include <dsmc/distribution/cell_point.h>
#include <dsmc/distribution/maxwell.h>
#include <dsmc/utils/generate_cloud.h>
#include <dsmc/utils/histogram.h>
#include <dsmc/utils/integrate.h>

int main()
{
    unsigned int const Nx = 400u;
    unsigned int const dim[3] = {Nx, 1u, 1u};
    dsmc::utils::BoundingBox const bbox =
    {
        {0., 0., 0.},
        {1., 1. / Nx, 1. / Nx},
    };
    dim::scalar<dim::Volume> const V = (bbox.max.x - bbox.min.x)
                                     * (bbox.max.y - bbox.min.y)
                                     * (bbox.max.z - bbox.min.z);

    dim::scalar<dim::None> const xHe = 0.97;
    dim::scalar<dim::None> const xXe = 0.03;
    dim::scalar<dim::Concentration> const n0 = 1.4e20;
    dim::scalar<dim::Temperature> const T0 = 273;
    dim::vector<dim::Velocity> const U0 = {1000, 0, 0};
    dim::vector<dim::Velocity> const U1 = {0, 0, 0};

    unsigned int const N0 = 500'000u;
    dim::scalar<dim::None> const statWeight = (n0 * V) / N0;

    Boundary boundary;
    std::get<0>(boundary) = dsmc::boundary::Inflow<dsmc::surface::XRectangle<true >, He, Xe>
    {
        .surface =
        {
            .r  = bbox.min,
            .dy = bbox.max.y - bbox.min.y,
            .dz = bbox.max.z - bbox.min.z,
        },
        .n = {xHe * n0, xXe * n0},
        .statWeight = statWeight,
        .T = T0,
        .U = U0,
    };
    std::get<1>(boundary) = dsmc::boundary::Inflow<dsmc::surface::XRectangle<false>, He, Xe>
    {
        .surface =
        {
            .r  = {bbox.max.x, bbox.min.y, bbox.min.z},
            .dy = bbox.max.y - bbox.min.y,
            .dz = bbox.max.z - bbox.min.z,
        },
        .n = {xHe * n0, xXe * n0},
        .statWeight = statWeight,
        .T = T0,
        .U = U1,
    };
    std::get<2>(boundary) = dsmc::boundary::Symmetry<1>
    {
        .r0 = bbox.min,
        .r1 = bbox.max,
    };
    std::get<3>(boundary) = dsmc::boundary::Symmetry<2>
    {
        .r0 = bbox.min,
        .r1 = bbox.max,
    };

    Mesh mesh = {bbox, dim};

    dsmc::Solver solver;
    using Cell = dsmc::Cell<He, Xe>;

    auto const cellRange = std::views::iota(0u, mesh.cells()) | std::views::transform
    (
        [&](Mesh::idx_t const cellID) noexcept
            -> Cell
        {
            Cell cell(statWeight / mesh.cellVolume(cellID));
            dsmc::distribution::CellPoint<Mesh> const pD = {mesh, cellID};
            std::size_t const NHe = dim::cast<std::size_t>(n0 * xHe / cell.weight + 0.5);
            std::size_t const NXe = dim::cast<std::size_t>(n0 * xXe / cell.weight + 0.5);
            dim::vector<dim::Velocity> const U = cellID * 5 < Nx ? U0 : U1;
            cell.cloud<He>() = dsmc::utils::generateCloud<He>(pD, dsmc::distribution::Maxwell<He>{T0, U}, NHe);
            cell.cloud<Xe>() = dsmc::utils::generateCloud<Xe>(pD, dsmc::distribution::Maxwell<Xe>{T0, U}, NXe);
            return cell;
        }
    ) | std::views::common;

    std::vector<Cell> cell =
    {
        std::ranges::begin(cellRange),
        std::ranges::  end(cellRange),
    };

    std::size_t particleCount = 0;
    for(Cell const &c : cell)
    {
        particleCount += c.cloud<He>().size();
        particleCount += c.cloud<Xe>().size();
    }
    std::cout << "W = " << statWeight.value << ", N = " << particleCount << std::endl;

    std::array<std::array<dim::float_t, 17>, Nx> table;
    for(auto &line : table)
        for(dim::float_t &f : line)
            f = 0;
    unsigned int accum = 0;
    auto const accumulate = [&]() noexcept
    {
        for(Mesh::idx_t const cellID : std::views::iota(0u, mesh.cells()))
        {
            auto const statsHe = dsmc::utils::integrate(cell[cellID].cloud<He>(), cell[cellID].weight);
            auto const statsXe = dsmc::utils::integrate(cell[cellID].cloud<Xe>(), cell[cellID].weight);
            dim::scalar<dim::Concentration> const n[2] =
            {
                statsHe.n,
                statsXe.n,
            };
            dim::scalar<dim::None> const x[2] =
            {
                n[0] / (n[0] + n[1]),
                n[1] / (n[0] + n[1]),
            };
            auto const T = (statsHe.p + statsXe.p) / (n[0] + n[1]) / dim::constant::k_B;

            table[cellID][0] += x[0].value;
            table[cellID][1] += x[1].value;
            table[cellID][2] += (n[0] + n[1]).value;
            table[cellID][3] += statsHe.rho.value;
            table[cellID][4] += statsXe.rho.value;
            table[cellID][5] += (statsHe.rho + statsXe.rho).value;
            table[cellID][6] += statsHe.T.value;
            table[cellID][7] += statsXe.T.value;
            table[cellID][8] += T.value;
            table[cellID][9] += statsHe.u.x.value;
            table[cellID][10] += statsXe.u.x.value;
            table[cellID][11] += statsHe.u.x.value * statsHe.rho.value;
            table[cellID][12] += statsXe.u.x.value * statsXe.rho.value;
            table[cellID][13] += statsHe.p.value;
            table[cellID][14] += statsXe.p.value;
            table[cellID][15] += statsHe.q.x.value;
            table[cellID][16] += statsXe.q.x.value;
        }
        ++accum;
    };

    dim::scalar<dim::Time> t = 0;
    auto const stepTo = [&](dim::scalar<dim::Time> const t0) noexcept
    {
        dim::scalar<dim::Time> dt = 1e-8;
        while(t < t0)
        {
            std::cerr << "t = " << t;
            std::cerr << "       \r";
            dim::scalar<dim::None> const ratio = solver.step<CollisionModel>(dt, boundary, mesh, cell);
            t += dt;
            dt = std::min((9 * dt + 0.5 * dt / ratio) / 10, t0 - t);
        }
    };

    for(unsigned int i = 0; i < 100; ++i)
    {
        if(i != 0)
            cell = std::vector<Cell>
            {
                std::ranges::begin(cellRange),
                std::ranges::  end(cellRange),
            };

        stepTo(3e-4);
        accumulate();
        t = 0;
    }

    std::cout << std::endl;

    std::ofstream out("out");
    for(Mesh::idx_t const cellID : std::views::iota(0u, mesh.cells()))
    {
        auto const &[min, max] = mesh.boundingBox(cellID);
        auto x = min.x;
        for([[maybe_unused]] auto k : std::views::iota(0, 2))
        {
            out << x.value;
            for(dim::float_t const f : table[cellID])
                out << " " << f / accum;
            out << std::endl;
            x = max.x;
        }
    }
}
