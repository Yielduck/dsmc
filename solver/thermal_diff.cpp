#include <iostream>
#include <fstream>

#include "table.h"

#include <dim/write.h>

#include <dsmc/solver.h>

#include <dsmc/collision_model/variable_soft_sphere.h>
using CollisionModel = dsmc::collision_model::VariableSoftSphere<Table>;

#include <dsmc/boundary/collection.h>
#include <dsmc/boundary/diffuse.h>
#include <dsmc/boundary/symmetry.h>
#include <dsmc/surface/xrectangle.h>
using Boundary = dsmc::boundary::Collection
<
    dsmc::boundary::Diffuse<dsmc::surface::XRectangle<true >>,
    dsmc::boundary::Diffuse<dsmc::surface::XRectangle<false>>,
    dsmc::boundary::Symmetry<1>,
    dsmc::boundary::Symmetry<2>
>;

#include <dsmc/mesh/cube.h>
using Mesh = dsmc::mesh::Cube;

#include <dsmc/distribution/cell_point.h>
#include <dsmc/distribution/maxwell.h>
#include <dsmc/utils/generate_cloud.h>
#include <dsmc/utils/histogram.h>
#include <dsmc/utils/integrate.h>

int main()
{
    unsigned int const Nx = 400u;
    unsigned int const dim[3] = {Nx, 1u, 1u};
    dsmc::utils::BoundingBox const bbox =
    {
        {0., 0., 0.},
        {1., 1. / Nx, 1. / Nx},
    };
    dim::scalar<dim::Volume> const V = (bbox.max.x - bbox.min.x)
                                     * (bbox.max.y - bbox.min.y)
                                     * (bbox.max.z - bbox.min.z);

    dim::scalar<dim::Concentration> const n0 = 1e20;
    dim::scalar<dim::Concentration> const nHe = n0 * 0.371;
    dim::scalar<dim::Concentration> const nAr = n0 - nHe;
    dim::scalar<dim::Temperature> const T0 = 1150;
    dim::vector<dim::Velocity> const U0 = {0, 0, 0};

    unsigned int const N0 = 50'000u;
    dim::scalar<dim::None> const statWeight = (n0 * V) / N0;

    Boundary boundary;
    std::get<0>(boundary) = dsmc::boundary::Diffuse<dsmc::surface::XRectangle<true >>
    {
        .surface =
        {
            .r  = bbox.min,
            .dy = bbox.max.y - bbox.min.y,
            .dz = bbox.max.z - bbox.min.z,
        },
        .T = 300,
        .U = {0, 0, 0},
    };
    std::get<1>(boundary) = dsmc::boundary::Diffuse<dsmc::surface::XRectangle<false>>
    {
        .surface =
        {
            .r  = {bbox.max.x, bbox.min.y, bbox.min.z},
            .dy = bbox.max.y - bbox.min.y,
            .dz = bbox.max.z - bbox.min.z,
        },
        .T = 2000,
        .U = {0, 0, 0},
    };
    std::get<2>(boundary) = dsmc::boundary::Symmetry<1>
    {
        .r0 = bbox.min,
        .r1 = bbox.max,
    };
    std::get<3>(boundary) = dsmc::boundary::Symmetry<2>
    {
        .r0 = bbox.min,
        .r1 = bbox.max,
    };

    Mesh mesh = {bbox, dim};

    dsmc::Solver solver;
    using Cell = dsmc::Cell<He, Ar>;

    auto const cellRange = std::views::iota(0u, mesh.cells()) | std::views::transform
    (
        [&](Mesh::idx_t const cellID) noexcept
            -> Cell
        {
            Cell cell(statWeight / mesh.cellVolume(cellID));

            dsmc::distribution::CellPoint<Mesh> const pD = {mesh, cellID};

            std::size_t const NHe = dim::cast<std::size_t>(nHe / cell.weight + 0.5);
            std::size_t const NAr = dim::cast<std::size_t>(nAr / cell.weight + 0.5);

            cell.cloud<He>() = dsmc::utils::generateCloud<He>(pD, dsmc::distribution::Maxwell<He>{T0, U0}, NHe);
            cell.cloud<Ar>() = dsmc::utils::generateCloud<Ar>(pD, dsmc::distribution::Maxwell<Ar>{T0, U0}, NAr);
            return cell;
        }
    ) | std::views::common;

    std::vector<Cell> cell =
    {
        std::ranges::begin(cellRange),
        std::ranges::  end(cellRange),
    };

    std::size_t particleCount = 0;
    for(Cell const &c : cell)
    {
        particleCount += c.cloud<He>().size();
        particleCount += c.cloud<Ar>().size();
    }
    std::cout << "W = " << statWeight.value << ", N = " << particleCount << std::endl;

    std::array<std::array<dim::float_t, 15>, Nx> table;
    for(auto &line : table)
        for(dim::float_t &f : line)
            f = 0;
    unsigned int accum = 0;
    auto const accumulate = [&]() noexcept
    {
        for(Mesh::idx_t const cellID : std::views::iota(0u, mesh.cells()))
        {
            auto const statsHe = dsmc::utils::integrate(cell[cellID].cloud<He>(), cell[cellID].weight);
            auto const statsAr = dsmc::utils::integrate(cell[cellID].cloud<Ar>(), cell[cellID].weight);
            dim::scalar<dim::Concentration> const n[2] =
            {
                statsAr.n,
                statsHe.n,
            };
            dim::scalar<dim::None> const X[2] =
            {
                n[0] / (n[0] + n[1]),
                n[1] / (n[0] + n[1]),
            };
            dim::scalar<dim::Velocity> const u[2] =
            {
                statsAr.u.x,
                statsHe.u.x,
            };
            dim::scalar<dim::MassFlux> const j[2] =
            {
                u[0] * statsAr.rho,
                u[1] * statsHe.rho,
            };

            table[cellID][0] += n[0].value;
            table[cellID][1] += n[1].value;
            table[cellID][2] += X[0].value;
            table[cellID][3] += X[1].value;
            table[cellID][4] += u[0].value;
            table[cellID][5] += u[1].value;
            table[cellID][6] += j[0].value;
            table[cellID][7] += j[1].value;
            table[cellID][8] += statsAr.T.value;
            table[cellID][9] += statsHe.T.value;
            table[cellID][10] += statsAr.p.value;
            table[cellID][11] += statsHe.p.value;
            table[cellID][12] += statsAr.p.value + statsHe.p.value;
            table[cellID][13] += statsAr.q.x.value;
            table[cellID][14] += statsHe.q.x.value;
        }
        ++accum;
    };

    dim::scalar<dim::Time> t = 0;
    auto const stepTo = [&](dim::scalar<dim::Time> const t0) noexcept
    {
        dim::scalar<dim::Time> dt = 1e-6;
        while(t < t0)
        {
            dim::scalar<dim::None> const ratio = solver.step<CollisionModel>(dt, statWeight, boundary, mesh, cell);
            std::cerr << "t = " << t << ", ratio = " << ratio << ", dt = " << dt;
            std::cerr << "       \r";
            t += dt;
            dt = std::min((9 * dt + 0.7 * dt / ratio) / 10, t0 - t);
        }
    };

    stepTo(6.);
    while(t < 8.)
    {
        stepTo(t + 1e-4);
        accumulate();
    }

    std::cout << std::endl;

    std::ofstream out("out");
    for(Mesh::idx_t const cellID : std::views::iota(0u, mesh.cells()))
    {
        auto const &[min, max] = mesh.boundingBox(cellID);
        auto x = min.x;
        for([[maybe_unused]] auto k : std::views::iota(0, 2))
        {
            out << x.value;
            for(dim::float_t const f : table[cellID])
                out << " " << f / accum;
            out << std::endl;
            x = max.x;
        }
    }
}
