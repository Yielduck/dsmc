#include <dsmc/boundary/collection.h>
#include <dsmc/boundary/specular.h>
#include <dsmc/boundary/symmetry.h>
#include <dsmc/distribution/maxwell.h>
#include <dsmc/surface/xrectangle.h>
#include <dsmc/utils/bounding_box.h>

#include <iostream>

struct S
{
    static constexpr dim::scalar<dim::Mass> molecularMass = 6e-27;
};

int main()
{
    dsmc::utils::BoundingBox const bbox =
    {
        {0., 0., 0.},
        {1., 1., 1.},
    };
    dsmc::boundary::Collection
    <
        dsmc::boundary::Specular<dsmc::surface::XRectangle<true >>,
        dsmc::boundary::Specular<dsmc::surface::XRectangle<false>>,
        dsmc::boundary::Symmetry<1>,
        dsmc::boundary::Symmetry<2>
    > boundary;
    std::get<0>(boundary) = dsmc::boundary::Specular<dsmc::surface::XRectangle<true>>
    {
        .surface =
        {
            .r  = bbox.min,
            .dy = bbox.max.y - bbox.min.y,
            .dz = bbox.max.z - bbox.min.z,
        },
    };
    std::get<1>(boundary) = dsmc::boundary::Specular<dsmc::surface::XRectangle<false>>
    {
        .surface =
        {
            .r  = {bbox.max.x, bbox.min.y, bbox.min.z},
            .dy = bbox.max.y - bbox.min.y,
            .dz = bbox.max.z - bbox.min.z,
        },
    };
    std::get<2>(boundary) = dsmc::boundary::Symmetry<1>
    {
        .r0 = bbox.min,
        .r1 = bbox.max,
    };
    std::get<3>(boundary) = dsmc::boundary::Symmetry<2>
    {
        .r0 = bbox.min,
        .r1 = bbox.max,
    };

    auto const move = [](dsmc::Particle<S> p, dim::scalar<dim::Time> t, auto &b) noexcept
        -> dsmc::Particle<S>
    {
        while(true)
        {
            std::optional<dsmc::surface::Hit> const optHit = b.surfaceHit(p.r, p.v);
            if(static_cast<bool>(optHit) == false || optHit->t > t)
                return
                {
                     .r = p.r + p.v * t,
                     .v = p.v,
                };
            std::cerr << p.r.x.value << " " << p.r.y.value << " " << p.r.z.value << " ";
            auto const &[escaped, dt, r, v] = b.hit(p);
            t   -= dt;
            p.r  = r;
            p.v  = v;
            std::cerr << p.r.x.value << " " << p.r.y.value << " " << p.r.z.value << std::endl;
        }
    };

    dsmc::Particle<S> p =
    {
        .r = {0.5, 0.5, 0.5},
        .v = dsmc::distribution::Maxwell<S>(273)(dsmc::utils::rng),
    };
    dim::scalar<dim::Time> const dt = 1e-4;
    dim::scalar<dim::Time> t = 0;
    while(t < 1e-2)
    {
        p = move(p, dt, boundary);
        std::cout << p.r.x.value << " " << p.r.y.value << " " << p.r.z.value << std::endl;
        t += dt;
    }
}
