#pragma once
#include <dsmc/mixture.h>
struct A
{
    static constexpr dim::scalar<dim::Mass> molecularMass = 8e-26;
};
struct B
{
    static constexpr dim::scalar<dim::Mass> molecularMass = 4e-26;
};
struct C
{
    static constexpr dim::scalar<dim::Mass> molecularMass = 1e-26;
};
struct Table
{
    using Mixture = dsmc::Mixture<A, B, C>;

    static constexpr pp::type_index N = Mixture::substanceCount;

    template<dsmc::concepts::Substance S>
    static constexpr pp::type_index I = Mixture::template indexOf<S>;

    static constexpr dim::scalar<dim::Length> dRef[N] =
    {
        4.16216e-10,
        3.49995e-10,
        2.47484e-10,
    };
    static constexpr dim::scalar<dim::Temperature> TRef[N][N] =
    {
        {273.15, 273.15, 273.15},
        {273.15, 273.15, 273.15},
        {273.15, 273.15, 273.15},
    };
    static constexpr dim::scalar<dim::None> omegaRef[N][N] =
    {
        {0.5, 0.5, 0.5},
        {0.5, 0.5, 0.5},
        {0.5, 0.5, 0.5},
    };
    static constexpr dim::scalar<dim::None> alphaRef[N][N] =
    {
        {1, 1, 1},
        {1, 1, 1},
        {1, 1, 1},
    };

    template<dsmc::concepts::Substance S1, dsmc::concepts::Substance S2>
    static constexpr dim::scalar<dim::Length> diameter = (dRef[I<S1>] + dRef[I<S2>]) / 2;
    template<dsmc::concepts::Substance S1, dsmc::concepts::Substance S2>
    static constexpr dim::scalar<dim::Temperature> temperature = TRef[I<S1>][I<S2>];
    template<dsmc::concepts::Substance S1, dsmc::concepts::Substance S2>
    static constexpr dim::scalar<dim::None> omega = omegaRef[I<S1>][I<S2>];
    template<dsmc::concepts::Substance S1, dsmc::concepts::Substance S2>
    static constexpr dim::scalar<dim::None> alpha = alphaRef[I<S1>][I<S2>];
};
