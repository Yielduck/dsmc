#include <iostream>
#include <fstream>

#include "table.h"

#include <dim/write.h>

#include <dsmc/solver.h>

#include <dsmc/collision_model/variable_soft_sphere.h>
using CollisionModel = dsmc::collision_model::VariableSoftSphere<Table>;

#include <dsmc/boundary/collection.h>
#include <dsmc/boundary/diffuse.h>
#include <dsmc/boundary/symmetry.h>
#include <dsmc/surface/xrectangle.h>
using Boundary = dsmc::boundary::Collection
<
    dsmc::boundary::Diffuse<dsmc::surface::XRectangle<true >>,
    dsmc::boundary::Diffuse<dsmc::surface::XRectangle<false>>,
    dsmc::boundary::Symmetry<1>,
    dsmc::boundary::Symmetry<2>
>;

#include <dsmc/mesh/cube.h>
using Mesh = dsmc::mesh::Cube;

#include <dsmc/distribution/cell_point.h>
#include <dsmc/distribution/maxwell.h>
#include <dsmc/utils/generate_cloud.h>
#include <dsmc/utils/histogram.h>
#include <dsmc/utils/integrate.h>

int main()
{
    unsigned int const Nx = 50u;
    unsigned int const dim[3] = {Nx, 1u, 1u};
    dsmc::utils::BoundingBox const bbox =
    {
        {0., 0., 0.},
        {1., 1. / Nx, 1. / Nx},
    };
    dim::scalar<dim::Volume> const V = (bbox.max.x - bbox.min.x)
                                     * (bbox.max.y - bbox.min.y)
                                     * (bbox.max.z - bbox.min.z);

    dim::scalar<dim::Concentration> const n0 = 1.4e20;
    dim::scalar<dim::Temperature> const T0 = 273;
    dim::vector<dim::Velocity> const U0 = {0, 0, 0};

    unsigned int const N0 = 100'000u;
    dim::scalar<dim::None> const statWeight = (n0 * V) / N0;

    Boundary boundary;
    std::get<0>(boundary) = dsmc::boundary::Diffuse<dsmc::surface::XRectangle<true >>
    {
        .surface =
        {
            .r  = bbox.min,
            .dy = bbox.max.y - bbox.min.y,
            .dz = bbox.max.z - bbox.min.z,
        },
        .T = T0,
        .U = {0, 0, 0},
    };
    std::get<1>(boundary) = dsmc::boundary::Diffuse<dsmc::surface::XRectangle<false>>
    {
        .surface =
        {
            .r  = {bbox.max.x, bbox.min.y, bbox.min.z},
            .dy = bbox.max.y - bbox.min.y,
            .dz = bbox.max.z - bbox.min.z,
        },
        .T = T0,
        .U = {0, 1000, 0},
    };
    std::get<2>(boundary) = dsmc::boundary::Symmetry<1>
    {
        .r0 = bbox.min,
        .r1 = bbox.max,
    };
    std::get<3>(boundary) = dsmc::boundary::Symmetry<2>
    {
        .r0 = bbox.min,
        .r1 = bbox.max,
    };

    Mesh mesh = {bbox, dim};

    dsmc::Solver solver;
    using Cell = dsmc::Cell<Ar>;

    auto const nAr = std::views::iota(0u) | std::views::transform
    ([n0](Mesh::idx_t) noexcept -> dim::scalar<dim::Concentration> {
        return n0;
    });

    dsmc::utils::RNG rng(0);
    auto const cellRange = std::views::iota(0u, mesh.cells()) | std::views::transform
    (
        [&](Mesh::idx_t const cellID) noexcept
            -> Cell
        {
            Cell cell(statWeight / mesh.cellVolume(cellID));
            dsmc::distribution::CellPoint<Mesh> const pD = {mesh, cellID};
            std::size_t const NAr = dim::cast<std::size_t>(nAr[cellID] / cell.weight + 0.5);

            cell.cloud<Ar>() = dsmc::utils::generateCloud<Ar>(pD, dsmc::distribution::Maxwell<Ar>{T0, U0}, NAr, rng);
            return cell;
        }
    ) | std::views::common;

    std::vector<Cell> cell =
    {
        std::ranges::begin(cellRange),
        std::ranges::  end(cellRange),
    };
    auto const TR = cell | std::views::transform
    (
        [&](auto const &c) noexcept
        {
            return dsmc::utils::integrate(c.template cloud<Ar>(), c.weight).T;
        }
    );
    dim::scalar<dim::Temperature> Tsum = 0;
    for(dim::scalar<dim::Temperature> const T : TR)
        Tsum += T;
    std::cout << "Tmean = " << Tsum / mesh.cells() << ", ";

    std::size_t particleCount = 0;
    for(Cell const &c : cell)
    {
        particleCount += c.cloud<Ar>().size();
    }
    std::cout << "W = " << statWeight.value << ", N = " << particleCount << std::endl;

    std::array<std::array<dim::float_t, 9>, Nx> table;
    for(auto &line : table)
        for(dim::float_t &f : line)
            f = 0;
    unsigned int accum = 0;
    auto const accumulate = [&]() noexcept
    {
        for(Mesh::idx_t const cellID : std::views::iota(0u, mesh.cells()))
        {
            auto const statsAr = dsmc::utils::integrate(cell[cellID].cloud<Ar>(), cell[cellID].weight);
            dim::vector<dim::Velocity> const u = statsAr.u;

            table[cellID][0] += statsAr.n.value;
            table[cellID][1] += statsAr.T.value;
            table[cellID][2] += u.x.value;
            table[cellID][3] += u.y.value;
            table[cellID][4] += dim::length(u).value;
            table[cellID][5] += statsAr.p.value;
            table[cellID][6] += statsAr.rho.value;
            table[cellID][7] += statsAr.q.x.value;
            table[cellID][8] += statsAr.P.x.y.value;
        }
        ++accum;
    };

    /*
    dim::scalar<dim::Temperature> const TMax = 440;
    dim::scalar<dim::Velocity> const vT = dim::sqrt(2 * dim::constant::k_B * TMax / Ar::molecularMass);
    dim::scalar<dim::Length> const L = 0.00925;
    dim::scalar<dim::Time> const tL = L / vT;
    */
    dim::scalar<dim::Time> const dt = 1e-6;

    dim::scalar<dim::Time> t = 0;
    auto const stepOver = [&](dim::scalar<dim::Time> const t0) noexcept
    {
        while(t < t0)
        {
            dim::scalar<dim::None> const ratio = solver.step<CollisionModel>(dt, boundary, mesh, cell);
            std::cerr << "t = " << t << ", ratio = " << ratio << ", dt = " << dt;
            std::cerr << "       \r";
            t += dt;
            //dt = (99 * dt + 0.25 * dt / ratio) / 100;
        }
    };
    auto const print = [&](char const filename[]) noexcept
    {
        std::ofstream out(filename);
        for(Mesh::idx_t const cellID : std::views::iota(0u, mesh.cells()))
        {
            auto const &[min, max] = mesh.boundingBox(cellID);
            auto x = (min.x + max.x) / 2;
            out << x.value;
            for(dim::float_t const f : table[cellID])
                out << " " << f / accum;
            out << std::endl;
        }
    };

    stepOver(0.5);
    while(t < 1.)
    {
        stepOver(t + 5e-6);
        accumulate();
    }
    print("out");
}
