#include <iostream>
#include <dsmc/collision_model/generate_table.h>
int main()
{
    dim::scalar<dim::Temperature> const T0 = 273.15;
    dim::scalar<dim::Concentration> const n0 = dim::scalar<dim::Pressure>(1e5) / dim::constant::k_B / T0;
    dim::scalar<dim::div(dim::Area, dim::Time)> const D[] =
    {
        1e-5,
        2e-5,
        8e-5,
    };
    dim::scalar<dim::div(dim::Area, dim::Time)> const D2[] =
    {
        1.445596e-5,
        3.33704e-5,
        4.34049e-5,
    };
    dim::scalar<dim::Mass> const m[] =
    {
        8e-26,
        4e-26,
        1e-26,
    };
    dim::scalar<dim::None> const Sc = 5. / 6.;
    std::array const info =
    {
        dsmc::collision_model::SubstanceInfo
        {
            .name = "A",
            .mass = m[0],
            .TRef = T0,
            .muRef = Sc * m[0] * n0 * D[0],
            .omega = 0.5,
            .Sc = Sc,
        },
        dsmc::collision_model::SubstanceInfo
        {
            .name = "B",
            .mass = m[1],
            .TRef = T0,
            .muRef = Sc * m[1] * n0 * D[1],
            .omega = 0.5,
            .Sc = Sc,
        },
        dsmc::collision_model::SubstanceInfo
        {
            .name = "C",
            .mass = m[2],
            .TRef = T0,
            .muRef = Sc * m[2] * n0 * D[2],
            .omega = 0.5,
            .Sc = Sc,
        },
    };

    std::array const pairInfo =
    {
        dsmc::collision_model::SubstancePairInfo
        {
            .TRef = T0,
            .nRef = n0,
            .DRef = D2[0],
            .omega = 0.5,
        },
        dsmc::collision_model::SubstancePairInfo
        {
            .TRef = T0,
            .nRef = n0,
            .DRef = D2[1],
            .omega = 0.5,
        },
        dsmc::collision_model::SubstancePairInfo
        {
            .TRef = T0,
            .nRef = n0,
            .DRef = D2[2],
            .omega = 0.5,
        },
    };

    dsmc::collision_model::generateTable(info, pairInfo, std::cout);
}
