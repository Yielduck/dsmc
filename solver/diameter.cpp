#include <iostream>
#include <ranges>
#include <dim/constant.h>
#include <dsmc/collision_model/variable_soft_sphere.h>
#include "table.h"

using Model = dsmc::collision_model::VariableHardSphere<Table>;

int main()
{
    unsigned int const N = 500;
    dim::scalar<dim::Energy> const E0 = dim::constant::k_B * dim::scalar<dim::Temperature>(5000);
    dim::scalar<dim::None> const pi = std::numbers::pi;
    using S1 = He;
    using S2 = Ar;

    dim::scalar<dim::None> const alpha1 = Table::alpha<S1, S1>;
    dim::scalar<dim::None> const alpha2 = Table::alpha<S2, S2>;
    dim::scalar<dim::None> const alpha12 = Table::alpha<S1, S2>;
    dim::scalar<dim::None> const omega1 = Table::omega<S1, S1>;
    dim::scalar<dim::None> const omega2 = Table::omega<S2, S2>;
    dim::scalar<dim::None> const omega12 = Table::omega<S1, S2>;
    dim::scalar<dim::Mass> const m1 = S1::molecularMass;
    dim::scalar<dim::Mass> const m2 = S2::molecularMass;
    dim::scalar<dim::Mass> const m = S1::molecularMass * S2::molecularMass / (S1::molecularMass + S2::molecularMass);
    dim::scalar<dim::Temperature> const TRef = 273.15;
    dim::scalar<dim::Viscosity> const muRef1 = 1.865e-5;
    dim::scalar<dim::Viscosity> const muRef2 = 2.117e-5;
    dim::scalar<dim::div(dim::Area, dim::Time)> const DRef = 6.37e-5;
    dim::scalar<dim::Pressure> const p0 = 101325;
    dim::scalar<dim::Concentration> const nRef = p0 / dim::constant::k_B / TRef;

    for(unsigned int i : std::views::iota(50u, N))
    {
        dim::scalar<dim::Energy> const E = i * E0 / (N - 1);
        dim::scalar<dim::Length> const d1 = dim::sqrt
        (
            5 * (alpha1 + 1) * (alpha1 + 2)
          * dim::sqrt(E * m1 / pi)
          * dim::pow(dim::constant::k_B * TRef / E, omega1)
          / 16. / alpha1 / dim::tgamma(4.5 - omega1) / muRef1
        );
        dim::scalar<dim::Length> const d2 = dim::sqrt
        (
            5 * (alpha2 + 1) * (alpha2 + 2)
          * dim::sqrt(E * m2 / pi)
          * dim::pow(dim::constant::k_B * TRef / E, omega2)
          / 16. / alpha2 / dim::tgamma(4.5 - omega2) / muRef2
        );
        dim::scalar<dim::Length> const d12 = dim::sqrt
        (
            3. * (alpha12 + 1.)
          * dim::sqrt(2 * E / m / pi)
          * dim::pow(dim::constant::k_B * TRef / E, omega12)
          / 16. / dim::tgamma(3.5 - omega12) / nRef / DRef
        );

        std::cout << 10 * i << " ";
        std::cout << d1.value << " ";
        std::cout << d2.value << " ";
        std::cout << d12.value << " ";
        std::cout << 0.5 * (d1 + d2).value << " ";

        {
            dim::scalar<dim::Velocity> const cr = dim::sqrt(4 * E / m1);
            dsmc::Particle<S1> const p1 = {{0., 0., 0.}, { cr / 2, 0., 0.}};
            dsmc::Particle<S1> const p2 = {{0., 0., 0.}, {-cr / 2, 0., 0.}};
            dim::scalar<dim::Area> const S = Model::crossSection(p1, p2);
            dim::scalar<dim::Length> const D = dim::sqrt(S / pi);
            std::cout << D.value << " ";
        }
        {
            dim::scalar<dim::Velocity> const cr = dim::sqrt(4 * E / m2);
            dsmc::Particle<S2> const p1 = {{0., 0., 0.}, { cr / 2, 0., 0.}};
            dsmc::Particle<S2> const p2 = {{0., 0., 0.}, {-cr / 2, 0., 0.}};
            dim::scalar<dim::Area> const S = Model::crossSection(p1, p2);
            dim::scalar<dim::Length> const D = dim::sqrt(S / pi);
            std::cout << D.value << " ";
        }
        {
            dim::scalar<dim::Velocity> const cr = dim::sqrt(2 * E / m);
            dsmc::Particle<S1> const p1 = {{0., 0., 0.}, { cr * m / S1::molecularMass, 0., 0.}};
            dsmc::Particle<S2> const p2 = {{0., 0., 0.}, {-cr * m / S2::molecularMass, 0., 0.}};
            dim::scalar<dim::Area> const S = Model::crossSection(p1, p2);
            dim::scalar<dim::Length> const D = dim::sqrt(S / pi);
            std::cout << D.value << " ";
        }
        std::cout << std::endl;
    }
}
