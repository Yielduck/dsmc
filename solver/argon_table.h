#pragma once
#include <dsmc/mixture.h>
struct Ar
{
    static constexpr dim::scalar<dim::Mass> molecularMass = 6.64216e-26;
};
struct Ar2
{
    static constexpr dim::scalar<dim::Mass> molecularMass = 6.64216e-26;
};
struct Table
{
    using Mixture = dsmc::Mixture<Ar, Ar2>;

    static constexpr pp::type_index N = Mixture::substanceCount;

    template<dsmc::concepts::Substance S>
    static constexpr pp::type_index I = Mixture::template indexOf<S>;

    static constexpr dim::scalar<dim::Length> dRef[N] =
    {
        4.11089e-10,
        4.11089e-10,
    };
    static constexpr dim::scalar<dim::Temperature> TRef[N][N] =
    {
        {273.15, 273.15},
        {273.15, 273.15},
    };
    static constexpr dim::scalar<dim::None> omegaRef[N][N] =
    {
        {0.81, 0.81},
        {0.81, 0.81},
    };
    static constexpr dim::scalar<dim::None> alphaRef[N][N] =
    {
        {1.40148, 1.40148},
        {1.40148, 1.40148},
    };

    template<dsmc::concepts::Substance S1, dsmc::concepts::Substance S2>
    static constexpr dim::scalar<dim::Length> diameter = (dRef[I<S1>] + dRef[I<S2>]) / 2;
    template<dsmc::concepts::Substance S1, dsmc::concepts::Substance S2>
    static constexpr dim::scalar<dim::Temperature> temperature = TRef[I<S1>][I<S2>];
    template<dsmc::concepts::Substance S1, dsmc::concepts::Substance S2>
    static constexpr dim::scalar<dim::None> omega = omegaRef[I<S1>][I<S2>];
    template<dsmc::concepts::Substance S1, dsmc::concepts::Substance S2>
    static constexpr dim::scalar<dim::None> alpha = alphaRef[I<S1>][I<S2>];
};
