#include <iostream>
#include <fstream>

#include "abc_table.h"

#include <dim/write.h>

#include <dsmc/solver.h>

#include <dsmc/collision_model/variable_soft_sphere.h>
using CollisionModel = dsmc::collision_model::VariableSoftSphere<Table>;

#include <dsmc/boundary/collection.h>
#include <dsmc/boundary/control_surface.h>
#include <dsmc/boundary/inflow.h>
#include <dsmc/boundary/symmetry.h>
#include <dsmc/surface/xrectangle.h>
#include <dsmc/surface/xrect.h>
using Boundary = dsmc::boundary::Collection
<
    dsmc::boundary::Inflow<dsmc::surface::XRectangle<true >, A, B, C>,
    dsmc::boundary::Inflow<dsmc::surface::XRectangle<false>, A, B, C>,
    dsmc::boundary::Symmetry<1>,
    dsmc::boundary::Symmetry<2>,
    dsmc::boundary::ControlSurface<dsmc::surface::XRect, A, B, C>
>;

#include <dsmc/mesh/cube.h>
using Mesh = dsmc::mesh::Cube;

#include <dsmc/distribution/cell_point.h>
#include <dsmc/distribution/maxwell.h>
#include <dsmc/utils/generate_cloud.h>
#include <dsmc/utils/histogram.h>
#include <dsmc/utils/integrate.h>

int main()
{
    unsigned int const Nx = 200u;
    unsigned int const dim[3] = {Nx, 1u, 1u};
    dsmc::utils::BoundingBox const bbox =
    {
        {0., 0., 0.},
        {1., 1. / Nx, 1. / Nx},
    };
    dim::scalar<dim::Volume> const V = (bbox.max.x - bbox.min.x)
                                     * (bbox.max.y - bbox.min.y)
                                     * (bbox.max.z - bbox.min.z);

    dim::scalar<dim::Concentration> const n0 = 3e20;
    dim::scalar<dim::Temperature> const T0 = 273;
    dim::vector<dim::Velocity> const U0 = {0, 0, 0};

    dim::scalar<dim::None> const X0[3] = {0.5, 0.4, 0.1};
    dim::scalar<dim::None> const X1[3] = {0.1, 0.2, 0.7};

    unsigned int const N0 = 100'000u;
    dim::scalar<dim::None> const statWeight = (n0 * V) / N0;

    Boundary boundary;
    std::get<0>(boundary) = dsmc::boundary::Inflow<dsmc::surface::XRectangle<true >, A, B, C>
    {
        .surface =
        {
            .r  = bbox.min,
            .dy = bbox.max.y - bbox.min.y,
            .dz = bbox.max.z - bbox.min.z,
        },
        .n = {X0[0] * n0, X0[1] * n0, X0[2] * n0},
        .statWeight = statWeight,
        .T = T0,
        .U = {0, 0, 0},
    };
    std::get<1>(boundary) = dsmc::boundary::Inflow<dsmc::surface::XRectangle<false>, A, B, C>
    {
        .surface =
        {
            .r  = {bbox.max.x, bbox.min.y, bbox.min.z},
            .dy = bbox.max.y - bbox.min.y,
            .dz = bbox.max.z - bbox.min.z,
        },
        .n = {X1[0] * n0, X1[1] * n0, X1[2] * n0},
        .statWeight = statWeight,
        .T = T0,
        .U = {0, 0, 0},
    };
    std::get<2>(boundary) = dsmc::boundary::Symmetry<1>
    {
        .r0 = bbox.min,
        .r1 = bbox.max,
    };
    std::get<3>(boundary) = dsmc::boundary::Symmetry<2>
    {
        .r0 = bbox.min,
        .r1 = bbox.max,
    };
    auto &controlSurface = std::get<4>(boundary);
    controlSurface = dsmc::boundary::ControlSurface<dsmc::surface::XRect, A, B, C>
    {
        .surface =
        {
            .r  = (bbox.min + bbox.max) / 2,
            .dy = bbox.max.y - bbox.min.y,
            .dz = bbox.max.z - bbox.min.z,
        },
        .info =
        {
            {
                {0, 0},
                {0, 0},
            },
            {
                {0, 0},
                {0, 0},
            },
            {
                {0, 0},
                {0, 0},
            },
        },
    };

    Mesh mesh = {bbox, dim};

    dsmc::Solver solver;
    using Cell = dsmc::Cell<A, B, C>;

    auto const nA = std::views::iota(0u) | std::views::transform
    ([=](Mesh::idx_t const cellID) noexcept -> dim::scalar<dim::Concentration> {
        return n0 * (X0[0] + cellID * (X1[0] - X0[0]) / Nx);
    });
    auto const nB = std::views::iota(0u) | std::views::transform
    ([=](Mesh::idx_t const cellID) noexcept -> dim::scalar<dim::Concentration> {
        return n0 * (X0[1] + cellID * (X1[1] - X0[1]) / Nx);
    });
    auto const nC = std::views::iota(0u) | std::views::transform
    ([=](Mesh::idx_t const cellID) noexcept -> dim::scalar<dim::Concentration> {
        return n0 * (X0[2] + cellID * (X1[2] - X0[2]) / Nx);
    });

    dsmc::utils::RNG rng;
    auto const cellRange = std::views::iota(0u, mesh.cells()) | std::views::transform
    (
        [&](Mesh::idx_t const cellID) noexcept
            -> Cell
        {
            Cell cell(statWeight / mesh.cellVolume(cellID));

            dsmc::distribution::CellPoint<Mesh> const pD = {mesh, cellID};

            std::size_t const NA = dim::cast<std::size_t>(nA[cellID] / cell.weight + 0.5);
            std::size_t const NB = dim::cast<std::size_t>(nB[cellID] / cell.weight + 0.5);
            std::size_t const NC = dim::cast<std::size_t>(nC[cellID] / cell.weight + 0.5);

            cell.cloud<A>() = dsmc::utils::generateCloud<A>(pD, dsmc::distribution::Maxwell<A>{T0, U0}, NA, rng);
            cell.cloud<B>() = dsmc::utils::generateCloud<B>(pD, dsmc::distribution::Maxwell<B>{T0, U0}, NB, rng);
            cell.cloud<C>() = dsmc::utils::generateCloud<C>(pD, dsmc::distribution::Maxwell<C>{T0, U0}, NC, rng);
            return cell;
        }
    ) | std::views::common;

    std::vector<Cell> cell =
    {
        std::ranges::begin(cellRange),
        std::ranges::  end(cellRange),
    };

    std::size_t particleCount = 0;
    for(Cell const &c : cell)
    {
        particleCount += c.cloud<A>().size();
        particleCount += c.cloud<B>().size();
        particleCount += c.cloud<C>().size();
    }
    std::cout << "W = " << statWeight.value << ", N = " << particleCount << std::endl;

    std::array<std::array<dim::float_t, 18>, Nx> table;
    for(auto &line : table)
        for(dim::float_t &f : line)
            f = 0;
    unsigned int accum = 0;
    auto const accumulate = [&]() noexcept
    {
        for(Mesh::idx_t const cellID : std::views::iota(0u, mesh.cells()))
        {
            auto const statsA = dsmc::utils::integrate(cell[cellID].cloud<A>(), cell[cellID].weight);
            auto const statsB = dsmc::utils::integrate(cell[cellID].cloud<B>(), cell[cellID].weight);
            auto const statsC = dsmc::utils::integrate(cell[cellID].cloud<C>(), cell[cellID].weight);
            dim::scalar<dim::Concentration> const n[] =
            {
                statsA.n,
                statsB.n,
                statsC.n,
            };
            dim::scalar<dim::None> const X[] =
            {
                n[0] / (n[0] + n[1] + n[2]),
                n[1] / (n[0] + n[1] + n[2]),
                n[2] / (n[0] + n[1] + n[2]),
            };
            dim::scalar<dim::Velocity> const u[] =
            {
                statsA.u.x,
                statsB.u.x,
                statsC.u.x,
            };
            dim::scalar<dim::MassFlux> const j[] =
            {
                u[0] * statsA.rho,
                u[1] * statsB.rho,
                u[2] * statsC.rho,
            };

            table[cellID][ 0] += n[0].value;
            table[cellID][ 1] += n[1].value;
            table[cellID][ 2] += n[2].value;
            table[cellID][ 3] += X[0].value;
            table[cellID][ 4] += X[1].value;
            table[cellID][ 5] += X[2].value;
            table[cellID][ 6] += u[0].value;
            table[cellID][ 7] += u[1].value;
            table[cellID][ 8] += u[2].value;
            table[cellID][ 9] += j[0].value;
            table[cellID][10] += j[1].value;
            table[cellID][11] += j[2].value;
            table[cellID][12] += statsA.T.value;
            table[cellID][13] += statsB.T.value;
            table[cellID][14] += statsC.T.value;
            table[cellID][15] += statsA.q.x.value;
            table[cellID][16] += statsB.q.x.value;
            table[cellID][17] += statsC.q.x.value;
        }
        ++accum;
    };

    dim::scalar<dim::Time> t = 0;
    dim::scalar<dim::Time> dt = 2e-6;
    auto const stepOver = [&](dim::scalar<dim::Time> const t0) noexcept
    {
        while(t < t0)
        {
            dim::scalar<dim::None> const ratio = solver.step<CollisionModel>(dt, boundary, mesh, cell);
            std::cerr << "t = " << t << ", ratio = " << ratio << ", dt = " << dt;
            std::cerr << "       \r";
            t += dt;
            //dt = (99 * dt + 0.25 * dt / ratio) / 100; // keeps ratio near quarter
        }
    };

    stepOver(1.);
    dt = 5e-8;
    stepOver(1.2);
    for(unsigned int i = 0; i < 3; ++i)
        for(unsigned int j = 0; j < 2; ++j)
            controlSurface.info[i].hitCount[j] = 0;
    while(t < 1.5)
    {
        stepOver(t + 5e-6);
        accumulate();
    }

    dim::scalar<dim::None> const hitA[2] =
    {
        static_cast<dim::float_t>(controlSurface.info[0].hitCount[0]),
        static_cast<dim::float_t>(controlSurface.info[0].hitCount[1]),
    };
    dim::scalar<dim::None> const hitB[2] =
    {
        static_cast<dim::float_t>(controlSurface.info[1].hitCount[0]),
        static_cast<dim::float_t>(controlSurface.info[1].hitCount[1]),
    };
    dim::scalar<dim::None> const hitC[2] =
    {
        static_cast<dim::float_t>(controlSurface.info[2].hitCount[0]),
        static_cast<dim::float_t>(controlSurface.info[2].hitCount[1]),
    };
    std::cout << hitA[0] - hitA[1] << " " << hitB[0] - hitB[1] << " " << hitC[0] - hitC[1] << std::endl;
    std::cout << std::endl;

    std::ofstream out("out");
    for(Mesh::idx_t const cellID : std::views::iota(0u, mesh.cells()))
    {
        auto const &[min, max] = mesh.boundingBox(cellID);
        auto x = (min.x + max.x) / 2;
        out << x.value;
        for(dim::float_t const f : table[cellID])
            out << " " << f / accum;
        out << std::endl;
    }
}
