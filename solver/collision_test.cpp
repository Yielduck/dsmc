#include <iostream>
#include <dim/constant.h>
#include <dim/write.h>
#include <dsmc/collide.h>
#include <dsmc/collision_model/variable_soft_sphere.h>
#include <dsmc/distribution/bounding_box.h>
#include <dsmc/distribution/maxwell.h>
#include <dsmc/utils/generate_cloud.h>
#include <dsmc/utils/integrate.h>
#include <dsmc/utils/histogram.h>

#include "table.h"
#include <algorithm>

using Model = dsmc::collision_model::VariableHardSphere<Table>;

int main(/*int argc, char *argv[]*/)
{
    dsmc::utils::BoundingBox const bbox = {{0., 0., 0.}, {1., 1., 1.}};
    dim::scalar<dim::Volume> const V = 1;

    dim::scalar<dim::Concentration> nAr = 1e20;
    dim::scalar<dim::Concentration> nHe = 2e20;
    unsigned int const N = 100000u;
    unsigned int const NHe = dim::cast<unsigned int>(N * nHe / (nAr + nHe));
    unsigned int const NAr = dim::cast<unsigned int>(N * nAr / (nAr + nHe));
    dim::scalar<dim::None> const statWeight = nAr * V / NAr;
    dim::scalar<dim::Temperature> const T0 = 273.15;
    dsmc::Cell<He, Ar> cell(statWeight / V);
    dsmc::utils::RNG rng;
    cell.cloud<He>() = dsmc::utils::generateCloud<He>
    (
        dsmc::distribution::BoundingBox{bbox},
        dsmc::distribution::Maxwell<He>{T0},
        NHe,
        rng
    );
    cell.cloud<Ar>() = dsmc::utils::generateCloud<Ar>
    (
        dsmc::distribution::BoundingBox{bbox},
        dsmc::distribution::Maxwell<Ar>{T0},
        NAr,
        rng
    );
    dsmc::collide<Model>(cell, 1e-3, rng);

    auto const statsHe = dsmc::utils::integrate(cell.cloud<He>(), cell.weight);
    auto const statsAr = dsmc::utils::integrate(cell.cloud<Ar>(), cell.weight);
    nHe = statsHe.n;
    nAr = statsAr.n;
    dim::scalar<dim::Temperature> const THA = (statsHe.p + statsAr.p) / (nHe + nAr) / dim::constant::k_B;

    dim::scalar<dim::Time> const dt = 1e-8;
    auto const D = 2 * std::numbers::pi * dim::constant::k_B;
    dim::scalar<dim::None> const collHe = 2. * Table::diameter<He, He> * Table::diameter<He, He>
                                             * nHe * nHe * dim::sqrt(D * Table::temperature<He, He> * 2 / He::molecularMass)
                                             * dim::pow(statsHe.T / Table::temperature<He, He>, 1 - Table::omega<He, He>)
                                             * V * dt / statWeight;
    dim::scalar<dim::None> const collAr = 2. * Table::diameter<Ar, Ar> * Table::diameter<Ar, Ar>
                                             * nAr * nAr * dim::sqrt(D * Table::temperature<Ar, Ar> * 2 / Ar::molecularMass)
                                             * dim::pow(statsAr.T / Table::temperature<Ar, Ar>, 1 - Table::omega<Ar, Ar>)
                                             * V * dt / statWeight;
    dim::scalar<dim::None> const collHA = 2. * Table::diameter<He, Ar> * Table::diameter<He, Ar>
                                             * nHe * nAr * dim::sqrt(D * Table::temperature<He, Ar> * (He::molecularMass + Ar::molecularMass) / He::molecularMass / Ar::molecularMass)
                                             * dim::pow(THA / Table::temperature<He, Ar>, 1 - Table::omega<He, Ar>)
                                             * V * dt / statWeight;
    dim::scalar<dim::None> const N0 = collHe + collHA;
    dim::scalar<dim::None> const N1 = collAr + collHA;
    std::cout << "equilibrium collision count: " << N0 << " " << N1 << std::endl;
    unsigned int const trials = 100000u;
    dim::scalar<dim::None> sum0 = 0;
    dim::scalar<dim::None> sum1 = 0;
    for(unsigned int i = 0; i < trials; ++i)
    {
        auto const count = dsmc::collide<Model>(cell, dt);
        sum0 += static_cast<dim::float_t>(count[0]);
        sum1 += static_cast<dim::float_t>(count[1]);
    }
    std::cout << "simulated collision count: " << sum0 / trials << " " << sum1 / trials << std::endl;
    std::cout << "ratio: " << sum0 / trials / N0 << " " << sum1 / trials / N1 << std::endl;
}
