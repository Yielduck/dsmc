#pragma once
#include <dsmc/mixture.h>
struct He
{
    static constexpr dim::scalar<dim::Mass> molecularMass = 6.64216e-27;
};
struct CO2
{
    static constexpr dim::scalar<dim::Mass> molecularMass = 7.30637e-26;
};
struct Ar
{
    static constexpr dim::scalar<dim::Mass> molecularMass = 6.64216e-26;
};
struct Table
{
    using Mixture = dsmc::Mixture<He, CO2, Ar>;

    static constexpr pp::type_index N = Mixture::substanceCount;

    template<dsmc::concepts::Substance S>
    static constexpr pp::type_index I = Mixture::template indexOf<S>;

    static constexpr dim::scalar<dim::Length> dRef[N] =
    {
        2.30073e-10,
        5.54602e-10,
        4.11089e-10,
    };
    static constexpr dim::scalar<dim::Temperature> TRef[N][N] =
    {
        {273.15, 273.15, 273.15},
        {273.15, 273.15, 273.15},
        {273.15, 273.15, 273.15},
    };
    static constexpr dim::scalar<dim::None> omegaRef[N][N] =
    {
        {0.66, 0.84, 0.725},
        {0.84, 0.93, 0.805},
        {0.725, 0.805, 0.81},
    };
    static constexpr dim::scalar<dim::None> alphaRef[N][N] =
    {
        {1.26437, 2.13054, 1.62254},
        {2.13054, 1.60913, 1.63166},
        {1.62254, 1.63166, 1.40148},
    };

    template<dsmc::concepts::Substance S1, dsmc::concepts::Substance S2>
    static constexpr dim::scalar<dim::Length> diameter = (dRef[I<S1>] + dRef[I<S2>]) / 2;
    template<dsmc::concepts::Substance S1, dsmc::concepts::Substance S2>
    static constexpr dim::scalar<dim::Temperature> temperature = TRef[I<S1>][I<S2>];
    template<dsmc::concepts::Substance S1, dsmc::concepts::Substance S2>
    static constexpr dim::scalar<dim::None> omega = omegaRef[I<S1>][I<S2>];
    template<dsmc::concepts::Substance S1, dsmc::concepts::Substance S2>
    static constexpr dim::scalar<dim::None> alpha = alphaRef[I<S1>][I<S2>];
};
