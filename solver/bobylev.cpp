#include <iostream>
#include <dim/constant.h>
#include <dim/write.h>
#include <dsmc/collide.h>
#include <dsmc/collision_model/variable_soft_sphere.h>
#include <dsmc/distribution/bounding_box.h>
#include <dsmc/distribution/bobylev.h>
#include <dsmc/utils/histogram.h>
#include <dsmc/utils/spawner.h>
#include <fstream>

struct S
{
    static constexpr dim::scalar<dim::Mass> molecularMass = 6.64e-26;
};
struct Table
{
    using Mixture = dsmc::Mixture<S>;

    template<dsmc::concepts::Substance S1, dsmc::concepts::Substance S2>
    static constexpr dim::scalar<dim::Length> diameter = 4.17e-10;
    template<dsmc::concepts::Substance S1, dsmc::concepts::Substance S2>
    static constexpr dim::scalar<dim::Temperature> temperature = 273.;
    template<dsmc::concepts::Substance S1, dsmc::concepts::Substance S2>
    static constexpr dim::scalar<dim::None> omega = 1.;
    template<dsmc::concepts::Substance S1, dsmc::concepts::Substance S2>
    static constexpr dim::scalar<dim::None> alpha = 2.1403;
};
using Model = dsmc::collision_model::VariableSoftSphere<Table>;

int main(/*int argc, char *argv[]*/)
{
    dsmc::utils::BoundingBox const bbox = {{0., 0., 0.}, {1., 1., 1.}};
    dim::scalar<dim::Volume> const V = 1;

    dim::scalar<dim::Concentration> const C = 1e20;
    unsigned int const N = 100000000u;
    dim::scalar<dim::None> const statWeight = C * V / N;
    dim::scalar<dim::None> const beta0 = 0.65;
    dim::scalar<dim::Temperature> const T0 = 273.;
    dsmc::Cell<S> cell(statWeight / V);
    dsmc::Cloud<S> &cloud = cell.cloud<S>();
    auto const spawner = dsmc::utils::spawner<S>
    (
        dsmc::distribution::BoundingBox{bbox},
        dsmc::distribution::Bobylev<S>{T0, beta0}
    );
    cloud.resize(N);
    std::ranges::generate(cloud, spawner);

    dim::scalar<dim::Frequency> const nuB = 5931.98;
    dim::scalar<dim::Time> const dt = 1e-5;

    auto const ktm = dim::constant::k_B * T0 / S::molecularMass;
    dim::scalar<dim::Frequency> const nu = 4. * Table::diameter<S, S> * Table::diameter<S, S>
                                              * C * dim::sqrt(std::numbers::pi * ktm);
    std::cout << "equilibrium collision count: " << (N * nu * dt).value << std::endl;

    unsigned int const writeStep = 5;
    for(unsigned int k = 0; k < dim::scalar<dim::Time>(3e-4) / dt; ++k)
    {
        if(k != 0)
            std::cout << "collisionCount = " << dsmc::collide<Model>(cell, dt)[0] << std::endl;

        dim::scalar<dim::None> const a = -nuB * dt * k;
        auto const beta = beta0 * dim::exp(a) / (1 + beta0 * (1 - dim::exp(a)));
        std::cout << "beta = " << beta << std::endl;

        if(k % writeStep == 0)
        {
            auto const hist = dsmc::utils::histogram
            (
                cloud | std::views::transform
                (
                    [](dsmc::Particle<S> const &p) noexcept
                    {
                        return p.v.x;
                    }
                ),
                dim::scalar<dim::Velocity>(-800),
                dim::scalar<dim::Velocity>( 800),
                500
            );

            auto const B = dsmc::distribution::Bobylev<S>{T0, beta};
            {
                std::ofstream out("out" + std::to_string(k));
                for(auto const &[v, f] : hist)
                {
                    auto const &[v0, v1] = v;
                    out << v0.value << " " << f.value << " " << B.pdf(v0.value).value << std::endl;
                    out << v1.value << " " << f.value << " " << B.pdf(v1.value).value << std::endl;
                }
            }
        }

        dim::scalar<dim::pow<4>(dim::Velocity)> v4 = 0.;
        for(auto const &[r, v] : cell.cloud<S>())
            for(unsigned int const i : std::views::iota(0u, 3u))
                v4 += v[i] * v[i] * v[i] * v[i];
        std::cerr << (dt * k).value << " ";
        std::cerr << (v4 / static_cast<double>(cloud.size() * 3)).value << " ";
        std::cerr << (3 * ktm * ktm * (1 + 2 * beta) / (1 + beta) / (1 + beta)).value << std::endl;
    }
}
