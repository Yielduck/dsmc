#include <iostream>
#include <dsmc/distribution/inflow.h>
#include <dsmc/utils/histogram.h>
#include "table.h"

using S = Ar;

dim::scalar<dim::Temperature> const T0 = 273;
dim::scalar<dim::Velocity> const vT = dim::sqrt(2 * dim::constant::k_B * T0 / S::molecularMass);

inline auto pdf(dim::scalar<dim::Velocity> const v, dim::scalar<dim::Velocity> const vn) noexcept
{
    auto const a = vn / vT;
    auto const z = v / vT;
    return 2 / vT * (a + z) * dim::exp(-z * z)
        / (dim::exp(-a * a) + a * std::sqrt(std::numbers::pi) * (1 + dim::erf(a)));
}

int main()
{
    dim::scalar<dim::Velocity> const vn = 500;
    auto D = dsmc::distribution::Inflow<S>({0., 0., 1.}, T0, {0., 0., vn});
    auto const hist = dsmc::utils::histogram
    (
        std::views::iota(0u, 1000000u) | std::views::transform
        (
            [&D](auto) noexcept
            {
                return D(dsmc::utils::rng).z;
            }
        ),
        dim::scalar<dim::Velocity>(0),
        dim::scalar<dim::Velocity>(1000 + vn)
    );
    for(auto const &[v, f] : hist)
    {
        auto const &[v0, v1] = v;
        std::cout << v0.value << " " << f.value << " " << pdf(v0 - vn, vn).value << std::endl;
        std::cout << v1.value << " " << f.value << " " << pdf(v1 - vn, vn).value << std::endl;
    }
}
